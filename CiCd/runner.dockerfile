FROM alpine:3.14

RUN apk add git \
            git-lfs \
            python3 \
            py3-pip \
            py3-wheel \
            py3-numpy \
            py3-h5py \
            py3-yaml


RUN pip3 install owlready2 coverage

ENV HDF5_DISABLE_VERSION_CHECK=1
