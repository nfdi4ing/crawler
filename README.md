# Metadata-Crawler

Welcome to the repository of <b>HOMER</b> the <b>H</b>PMC tool for <b>O</b>ntology-based <b>M</b>etadata <b>E</b>xtraction and <b>R</b>e-use. The Crawler was originally developed in the context of the "NFDI4Ing – the National Research Data Infrastructure for Engineering Sciences" project at the Chair of Aerodynamics and Fluid Mechanics of Technical University of Munich.

The crawler is capable to read out ontologies (.owl-files) and create a dictionary of relevant properties to be filled by the user. In a second step this dictionary is read-out to create a metadata-file which accompanies the main dataset. The dictionary therein may be filled with instruction where to find the relevant inputs rather than hard-coding them directly.

The project is still in very early development stages, so expect rapid changes in the near future.

In case of bugs or any problem in using HOMER please don't hesitate to contact the developers via Gitlab's 'Issue tracking system'. The same system can be used to make suggestion on how and to improve the code or to request specific feature to be added to the code. Anybody is invited to participate in order to make HOMER grow. To those who would like to contribute directly to the code by writing/adding their own routines, please get in touch with the developers first. The role of a contributor (see: authors) is defined by the DataCite Metadata Schema (https://schema.datacite.org/). The original authors reserve the right to decide, whether a contribution is substantial and contextual enough to get credited in the authors / CITATION file.

We hope the code is of help for you and are interested in your feedback.

## Releases
This git repository provides rolling updates. Major releases are also versioned and archived. The current relase is found [here](https://mediatum.ub.tum.de/1694401).

## Publications
The motivation behind HOMER, the code (version: 0.1.2) and the application are described in the article “From Ontology to Metadata: A Crawler for Script-based Workflows” (Chiapparino et al.): https://doi.org/10.48694/inggrid.3983.
Resulting study reports can be found in the [documentation](https://gitlab.lrz.de/nfdi4ing/crawler/-/tree/master/Documentation?ref_type=heads) folder.

## Basic installation and usage

Note `python|pip` refer to python3|pip3 commands.

We recommend using a virtual environment but this is optional
```
python -m venv venv
source venv/bin/activate
```

Installation is as simple as
```
pip install -r requirements.txt
python -m pip install .
```
Another option for this, which is however deprecated, is to use the following command: `python setup.py install`

Then to create a dictionary from an ontology
```
python build/lib/src/OwlImplementation/HOMER.py <Ontology.owl> <DictPath> -s 1
```
To use the multiplexer:

```
python3 build/lib/src/OwlImplementation/HOMER.py <OldDictPath> <NewDictPath> -s 2
```
The above steps can also be executed in one go when providing a config file:
```
python build/lib/src/OwlImplementation/HOMER.py <Ontology.owl> <OutputPath> -s 1 -c <ConfigPath>
```

and to parse the dictionary to create the metadata object.
```
python build/lib/src/OwlImplementation/HOMER.py <FilledDictPath> <OutputPath> -s 3 -c <ConfigPath>
```

if conflicts occur during the crawling process (i.e. multiple results for a specific property are found) a _conflict.json file will be generated.
The conflict can be resolved with:
```
python build/lib/src/CrawlConflictHandler.py <ConflictFile> <CrawledMetadataFile>
```

The path arguments to the newly generated files in the command above should include file endings (e.g '.json' or '.yaml'). Paths specified 
in the dictionary for the Step5 step are relative to the current working directory (i.e where python is begin run in). Additionally, all the 
scripts can also be run with the -h option to see a more detailed explanation.

You could also run the tests and see the coverage by running
```
python -m pytest tests/ --cov=src --cov-report='term-missing' --cov-config=tests/.coveragerc
```
To run this you must have the pytest and coverage packages installed. 
## Ontology
- GitLab: https://git.rwth-aachen.de/nfdi4ing/metadata4ing/metadata4ing
- HPMC Sub-ontology: https://git.rwth-aachen.de/nfdi4ing/metadata4ing/metadata4ing/-/tree/metadata4ing_hpmc?ref_type=heads 
- Publication: https://zenodo.org/record/5957104#.Y6RYVRWZOUm
- Documentation: https://nfdi4ing.pages.rwth-aachen.de/metadata4ing/metadata4ing/index.html

## Acknowledgements
The authors would like to thank the Federal Government and the Heads of Government of the Länder, as well as the Joint Science Conference (GWK), for their funding and support within the framework of the NFDI4Ing consortium. Funded by the German Research Foundation (DFG) project number 442146713.

The authors would also thank the Competence Network for Scientific High Performance Computing in Bavaria (KONWIHR) for their funding and support within a [short term project](https://www.konwihr.de/konwihr-projects/improving-fairness-of-hpc-research-data/).
