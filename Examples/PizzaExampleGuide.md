# Pizza Example Guide
## Installation
After cloning the repository and installing all dependencies, we can run the example. To do this, simply call 

> python3 ./build/lib/src/OwlImplementation/HOMER.py ./Examples/SimpleApplication_PizzaOntology/ontology.owl  output.json

The program will exit with an error message, but that's fine for now. You will also notice two files being created: output_step1.json and output_step2.json. These files contain the outputs of 
1. reading the ontology and extracting all its classes with placeholder values in places where the user might need to get active,
2. multiplexing the output of step 1 according to the values provided.

Before we tackle any of this, let's first get an overview of the early stage of the crawler.
## Getting an Overview
First off, we need to get an overview of the ontology. By default, all count values for the multiplexing stage are 1 or [1]. We can use the information on the ontology thus far gained in the next steps to slowly derive our final configuration file, which will completely automate the entire crawling process!

## Filling In Counts
In ye olden days, the user had to access output_step1.json and manually change all numbers they so desired to change. The structure of the file almost completely remained the same: To change the class count, one can simply edit the \_\_count\_\_ attribute of a given class. To change the count of a property, simply edit the integer list that follows the property name.

### What Are Counts? 
Counts are the number of instances of a given entity, that have to be crawled. If we say we have two instances of the class _Cheese_ in our example, we can set \_\_count\_\_ to 2. We then also have to adjust all property counts accordingly as <b>the length of the count list of a property has to match the \_\_count\_\_ attribute of the parent class</b>. So in our case, we also have to set "Main topping of" and "Topping of" to \[1, 1\]. This means that each instance of _Cheese_ has exactly one instance of each property. If the first instance of _Cheese_ were to have two instances of the property "Topping of" for example, we would set that to \[2, 1\].

### Introducing a Configuration File
Having to edit files by hand is highly inconvenient and we thus devined a method to make it a little less inconvenient. We can now start introducing a configuration file!

The simplest and most boring config is:
>exclude_classes = []

Save that in a .toml file (i.e. config.toml) and call
> python ./build/lib/src/OwlImplementation/HOMER.py ./Examples/SimpleApplication_PizzaOntology/ontology.owl  output.json --config ./config.toml

and you will make it to the next error message. You will notice that the program this time did not create any output files and that it exited with a different error message. When you set the exclusion filter to the empty list, all classes are to be crawled for. But since you already took it upon you to write that into a config instead of simply running the program without one, the program expects a little more effort from the users side. As you already made the wise choice of ditching the old ways and embracing the new, the program wants you to actually make use of your new powers. So, to get back to where we already were without the config, simply add two lines to make it the following:

> exclude_classes = [] <br>
default_count = 1 <br>
default_prop_count = \[1\]

And voila, we are back to where we started. Or are we? 

See, since we now have a sufficiently specified config, the program actually makes one complete run on the data provided. Which does not result in anything, but hey, progress. You can have a look at the newly generated output files output.json and output_conflict.json, but we will also get there in due time.

### Inheritance
Now might be a good point to talk about inheritance. As an ontology is basically a taxonomy, we have parent-child relationships between classes. All owlready2 classes are derived from the _Thing_ class and we could thus also write the same config as above as:

> exclude_classes = [] <br>
"Thing".count = 1 <br>
"Thing".default_prop_count = \[1\]

All our classes inherit these values from _Thing_ and we get the same result as with the default values. All so called crawler attributes, that is the path(s), pattern(s), type(s), and postprocessor(s) can also be inherited. This makes inheritance a powerful tool for those familiar with the structure of the underlying ontology.

## Step 1 of the Example
The actual pizza example we want to tackle in this guide has a slightly different start config. We begin the example with the config:
> include_classes = \["Vegetarian Pizza"\]

And we now call the program in the _interactive_ mode:
> python ./build/lib/src/OwlImplementation/HOMER.py ./Examples/SimpleApplication_PizzaOntology/ontology.owl  output.json --config ./config.toml --interactive 1

You will now be greeted by an input window that asks you for a class count for the class <i>Vegetarian Pizza</i>. We want to enter a 3 here, as we want to crawl three different instances of pizza later on. You are then prompted for the property counts, which you enter as a string of integers separated by a comma: 1, 1, 1. After hitting "OK", you will find a new output file called output_config.toml. This file contains a generated config based on the config you fed into the program and your own inputs for the prompts. You can use this config as a basis to play around some more, but most importantly, it is generated and updated after every successfull input form. You will thus only lose little progress in case anything happens.

But we now want to change the count of "Has topping" to \[0, 0, 0\]. You can do that either via calling the program again in interactive mode on the old config, or simply change the value in the newly generated config. If you try to call the interactive mode on the newly generated config, you will notice that you are actually no longer prompted for any inputs. That is because all counts are already well defined. The same situation also happens if you set default values. The interactive mode for step 1 will thus not prevent you from accidentally using default values. It will only prompt you for values that are yet unkown.

For the interested, the "space optimal" config would be:
> include_classes = \["Vegetarian Pizza"\] <br>
default_count = 3<br>
default_prop_count = \[1, 1, 1\]<br>
"Vegetarian Pizza".counts."Has topping" = \[0, 0, 0\]

Here again, you can play around with inheritance to get rid of the default values, or to push the definition of the "Has topping" count up to the "Pizza" class.

## Step 2 of the Example
Step 2 is a bit more complex with its requirements and inputs. You can call step 2 explicitly in the following way:
>  python ./build/lib/src/OwlImplementation/HOMER.py ./output_step1.json  output.json -s 2

This call will take output_step1.json as input and do the multiplexing step (-s 2) on it. Calling it with the ontology file instead of the output of step 1 will result in an error. The config we will derive in this section can also be passed over and form, together with the output of step 1, a complete specification for the crawler. But we want to have all the necessary information in one place and will thus continue extending our existing config.

We call the program again, this time using our config:
> python ./build/lib/src/OwlImplementation/HOMER.py ./Examples/SimpleApplication_PizzaOntology/ontology.owl  output.json --config ./config.toml --interactive 2

The first screen you see lets you select a class to edit, or to hit the "DONE" button and exit. Different from step 1, we do no longer have strict requirements on what has to be present in the config. After you selected <i>Vegetarian Pizza</i> as the class to edit, you are presented with yet again another selection. This time, you can select whether you want to edit the class's crawler attributes or the properties' crawler attributes.

### What Are Crawler Attributes?
Mentioned in passing in an earlier section, crawler attributes are the bulk of the config. They encompass the following topics: file paths, search patterns, parser types, and postprocessor types. <b>The postprocessors are currently not supported and will thus be ignored</b>! We also have to make a distinction between __class__ crawler attributes and __property__ crawler attributes. 

#### Class Crawler Attributes
Class crawler attributes are the default for any and all crawler attributes of any and all properties of a class. That means that the file paths specified as a class crawler attribute will be inherited by all the class's properties. These class crawler attributes are, for legacy reasons, named in singular:
- path: A list or a single string that represents a file path. Can contain as many wildcards as needed and can either be a file or a directory. Global file paths and cl arguments given will also be taken into account.
- pattern: A list or single string that represents a search pattern. Can be a regex, a cl command, or anything else supported by the crawler. For an empty pattern, the DefaultParser will assume the name of the entity currently crawled for as the search pattern inside of a default regex.
- type: A list or single string that represents a parser type. The parser will search through the files using the given search patterns. An empty parser type will be replaced by the DefaultParser.

All of these crawler attributes can be defined on two levels: On a class-wide level, meaning for all instances of the class, these values are to be taken; or on a class-instance level, meaning each instance has its own list of the respective class crawler attribute.

#### Property Crawler Attributes  
The property crawler attributes are overwriting the class crawler attributes when so desired. They are named in plural: paths, patterns, types. All of them can be defined on the previously mentioned two levels and additionally, also on a third, property-instance level. This will define a list of the respective crawler attribute for each instance of said property. 

In general, becoming more specific class-wide > class-instance > property-instance is allowed. However, becoming more unspecific is not possible. 

### Hands On
#### Filling In a Path
Now back to actually filling in stuff. We need file paths, search patterns, and pattern types for our example. Your first task is to fill in the file path as a class crawler attribute. All our <i>Vegetarian Pizza</i> instances are to use the same file as a source, so you can simply define that crawler attribute on a class-wide level. So, to do so, you will, from the class selection screen, where you selected <i>Vegetarian Pizza</i> for editing, select "CLASS Vegetarian Pizza" and hit "OK". Then select "PATH" and hit "EDIT". Next, confirm the already selected "CLASS-WIDE level" option and activate the "ADD PATH" button by selecting it with the arrow keys (or tabulator) and pressing enter. In the dialog, navigate to "./Examples/SimpleApplication_PizzaOntology/PizzaExample/Menus/RandomMenu_1.txt". Alternatively, you can also simply type "**/RandomMenu_1.txt" into the text area where your selected path should now appear. To get rid of the path again, simply delete it from the text area using backspace. If you want to select a directory, navigate into it and select a file inside. Then simply shorten the path in the text area. 

To successfully leave the form, hit the "OK" button. To cancel, simply hit the "CANCEL" button. Congratulations, you now have (hopefully) filled in your search path. It should now be displayed in the overview screen and if you open the output_config.toml file, you should also see it there.

#### Filling In Patterns
To fill in search patterns, we have to edit each property individualy (as there is no overarching search pattern). So go back to the main menu screen by hitting "BACK" and select <i>Vegetarian Pizza</i> again. Now, select any of the two properties and fill in the following pattern on a property-instance level:
- "Has main topping":<br>"Special First choice", "Special Second choice", "White First choice"
- "Price":<br>"Special First choice price", "Special Second choice price", "White First choice price"

You can do that by either typing each pattern into the respective text area, or by copy pasting them from this document.

## Step 3 of the Example
And with this, the config is finished. We do not have to specify any types for our parsers as the DefaultParser is the default for empty/non-existent types and that is exactly the parser we want to use. Congratulations, you have successfully crawled a simple file searching for simple information. But from this simplicity can arise great complexity, so try different inputs (especially dictionaries) and see for yourself what the program can or cannot do.

## Addendum
### Global Paths
You can specify global file paths by either entering a "paths = \["path/to/file"\] line into the config, or by using the '--startdir' flag. It is noteworthy that the handling of paths is a bit special from the other crawler attributes. If a global path is specified, this path takes the least precedence over the others. If a class path is set, it will take precedence only over all inherited paths and over global paths. If however a property path is set, this path will have highest precedence and also if a match has been found using such a path, the crawler will no longer search through the other paths. A property path is thus not only taking highest precedence, but also stopping the search. This is done in order that all matches to the search pattern can be captured if the user is not aware of in which file exactly the desired data is located. In all other cases, the user can either specify a single file for a class path, or supply the path as a property path and thus make the search stop after finding that match. 

Global paths are also not presented in the interactive mode as they are well localized in the program call or in the config. So inserting them in every input window for paths would have been too much. We expect the user to keep these global paths in mind and remember that they are never displayed anywhere inside the crawler.

### Inheritance without the Ontology
In step 2 of the example, we hinted at the option to fully configure the crawler by using the output of step 1 in conjunction with a config that specifies the crawler attributes. When doing so, full inheritance will only be usable when also supplying the underlying ontology via the '--ontology' flag. When not supplying an ontology, one-level inheritance can still be used since the direct parent classes of a class to crawl are given in the output of step 1. Thats also why we prefer to work directly with the ontology file and a full config.