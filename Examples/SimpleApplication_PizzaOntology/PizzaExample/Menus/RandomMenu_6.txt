******-- White Pizzas --******
White First choice: Quattro Formaggi
White First choice toppings: Mozzarella, scamorza, gorgonzola, parmigiano
White First choice price: 7.5
**----**
White Second choice: Focaccia
White Second choice toppings: Pizza bianca al sale
White Second choice price: 4.5
**----**
White Third choice: Cuppolone
White Third choice toppings: Mozzarella, scamorza, patate, speck, funghi, salsa bbq
White Third choice price: 7.5

*******-- Red Pizzas --*******
Red First choice: Margherita
Red First choice toppings: Pomodoro, mozzarella
Red First choice price: 5.5
**----**
Red Second choice: Capricciosa
Red Second choice toppings: Pomodoro, mozzarella, funghi, cotto, carciofini, olive, uovo
Red Second choice price: 7
**----**
Red Third choice: Villa Borghese
Red Third choice toppings: Pomodoro, mozzarella, verdure grigliate
Red Third choice price: 7

*****-- Special Pizzas --*****
Special First choice: Salmonata
Special First choice toppings: Salmone, patate, pesto di rucola
Special First choice price: 9
**----**
Special Second choice: Pariolina
Special Second choice toppings: Pomodoro in cottura, bufala, pachini, basilico, olio extravergine in uscita
Special Second choice price: 8
