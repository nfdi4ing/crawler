******-- White Pizzas --******
White First choice: Monterosa
White First choice toppings: Speck, scamorza
White First choice price: 7.5
**----**
White Second choice: Ruchetta
White Second choice toppings: Mozzarella, rucola, pachini, grana
White Second choice price: 6.5
**----**
White Third choice: Focaccia
White Third choice toppings: Pizza bianca al sale
White Third choice price: 4.5

*******-- Red Pizzas --*******
Red First choice: Margherita
Red First choice toppings: Pomodoro, mozzarella
Red First choice price: 5.5
**----**
Red Second choice: Villa Borghese
Red Second choice toppings: Pomodoro, mozzarella, verdure grigliate
Red Second choice price: 7
**----**
Red Third choice: Perugina
Red Third choice toppings: Pomodoro, mozzarella, salsiccia
Red Third choice price: 6.5

*****-- Special Pizzas --*****
Special First choice: Carbonara
Special First choice toppings: Mozzarella, guanciale, uovo, pecorino romano, pepe nero
Special First choice price: 7.5
**----**
Special Second choice: Vegana
Special Second choice toppings: Pomodoro, funghi porcini, carciofini, olive nere
Special Second choice price: 7
