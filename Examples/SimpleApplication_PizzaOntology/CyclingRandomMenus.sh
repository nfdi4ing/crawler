#!/bin/bash

echo Cycling through the menus!

# Define variables
n_cc=1 # Number of the current cycle (first value)
step=1 # Number of cycles to skip +1 - Set 1 if consecutive cycles are needed

# Copy original version of Step4 to a working file that will be modified in the cycles
cp PizzaExample/Cycle_Step4_Orig.json PizzaExample/Cycle_Step4.json

# Make a copy of the file specifying the cycle number
cp PizzaExample/Cycle_Step4_Orig.json PizzaExample/Cycle_Step4_File_Nr_"$n_cc".json

# Run the crawler for the first time
python3 OwlImplementation/Step5.py PizzaExample/Cycle_Step4.json PizzaExample/Cycle_Step5_c1.json

# Cycle over variables
while [ $n_cc -le 5 ] 
do
	# Update new-cycle counter
        ((n_nc=n_cc+step))

        # Modify target files in "path" spaces
        vim PizzaExample/Cycle_Step4.json -c %s/RandomMenu_"$n_cc"/RandomMenu_"$n_nc"/ -c 'wq' # change all lines where the menu number is used
	
	# Make a copy of the modified file specifying the cycle number
	cp PizzaExample/Cycle_Step4.json PizzaExample/Cycle_Step4_File_Nr_"$n_nc".json

	# Run the crawler
	python3 OwlImplementation/Step5.py PizzaExample/Cycle_Step4.json PizzaExample/Cycle_Step5.json

	# Change name of the crawled file according to the current cycle
        mv PizzaExample/Cycle_Step5.json PizzaExample/Cycle_Step5_c"$n_nc".json

	# Update current-cycle number
	((n_cc=n_nc))
done

# Move all files to a new directory
mkdir PizzaExample/ResultsCrawler
mv PizzaExample/Cycle_Step5_c* PizzaExample/ResultsCrawler/

echo All menus have been cycled through!

