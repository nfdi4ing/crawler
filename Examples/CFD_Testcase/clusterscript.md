#!/bin/bash
#SBATCH -o myjob.%x.%j.%N.out 
#SBATCH -D .
#SBATCH -J CEQ3-71sw
#SBATCH --get-user-env 
#SBATCH --clusters=cm2
#SBATCH --partition=cm2_std
#SBATCH --qos=cm2_std
#SBATCH --nodes=20
#SBATCH --ntasks-per-node=28
# multiples of 28 for mpp2
#SBATCH --mail-type=end,begin 
#SBATCH --mail-user=fritz@helmut-ulrich.eu
#SBATCH --export=NONE 
#SBATCH --time=10:00:00 

module load slurm_setup
module rm intel-mpi/2019-intel
module load openmpi/4.0.4-intel19
mpiexec --map-by ppr:1:core ./nsmb6.09.21.mpi_lrz_opt_ompi     #> TestA21_LRZ.out

