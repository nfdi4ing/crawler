"""
    Setup file for Crawler.
    Use setup.cfg to configure your project.

    This file was generated with PyScaffold 4.0.2.
    PyScaffold helps you to put up the scaffold of your new Python project.
    Learn more under: https://pyscaffold.org/
"""
from setuptools import setup, find_packages

setup(
    name="Crawler",
    version='0.1',
    author="Radoslav Ralev",
    author_email="radoslav.ralev@tum.de",
    packages=find_packages(include=['src','src.*']),
    install_requires=['owlready2', 'h5py', 'toml']
)
