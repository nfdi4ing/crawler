from src.UtilityClasses.FileIOWrappers import AbstractFileIO, TomlIO
from src.OwlImplementation.FillerShared import FillerWidget, FillerMultiPage

import npyscreen
import curses


class PropertyWidget(FillerWidget):
    def __init__(self, *args, count: int, **keywords):
        """!
        @param count The count of the corresponding class
        """
        self.count = count
        super().__init__(*args, **keywords)

    def safe_to_exit(self) -> bool:
        # check that string is not empty now, does not end with a space nor with a comma
        # as these are all involved in typing a list of comma separated integers
        try:
            input_list: list[int] = convert_input_list(self.value)
            if any(count < 0 for count in input_list):
                raise ValueError()
            if len(input_list) != self.count:
                raise ValueError()
            return True
        except ValueError:
            # Either the input was not a list of purely integers
            # or it contained negative numbers or its length was not sufficient
            self.show_brief_message(
                f"Invalid list of integers. All integers must be >= 0, separated by ',', and have a length of {self.count}!",
                time=2000,
            )
            return False


class ClassWidget(FillerWidget):
    def __init__(self, *args, config_defined: bool, **keywords):
        """!
        @param config_defined A bool whether or not a config had been defined previous to this filler instance.
        """
        self. config_defined = config_defined
        super().__init__(*args, **keywords)

    def safe_to_exit(self) -> bool:
        try:
            input = int(self.value)
            # allow for 0 in case no config was passed over to the program
            if input < (0 + self.config_defined):
                raise ValueError()
            return True
        except ValueError:
            self.show_brief_message(f'Invalid input. Must be an integer >{"=" if not self.config_defined else ""} 0!', time=2000)
            return False


class FillerStep1Form(FillerMultiPage):

    def __init__(self, *args, class_name: str, config: dict, preprocessing_cache: dict, **keywords):
        self.class_name = class_name
        self.config = config
        self.preproc_cache = preprocessing_cache
        super().__init__(*args, **keywords)

    def afterEditing(self):
        self.parentApp.finishEditing(self.class_name)


class ClassForm(FillerStep1Form):
    def __init__(self, *args, config_defined: bool, **keywords):
        """! Initializes a class form for a FillerStep1 application where the count of a class can be filled in.
        The expected arguments are:
        @param config_defined A bool whether or not a config had been defined previous to this filler instance.
        @param class_name The label of the owl class to work on.
        @param isLast True iff this form is the last in the list of forms.
        @param config The config as yielded by the initial toml file and updated by the FillerStep1 application.
        @param preprocessing_cache The preprocessing cache dict yielded by the OntologyBuilder's preprocessing.
        """
        self.config_defined = config_defined
        super().__init__(*args, **keywords)

    def create(self):
        self.add_instructions()

        self.add_separator()

        self.class_widget: ClassWidget = self.add_widget_intelligent(
            ClassWidget, name=f'Class "{self.class_name.upper()}" count: ', value=0, config_defined=self.config_defined,
        )

        self.add_separator()

        self.add_ok_button()

    def on_ok(self):
        try:
            value = int(self.class_widget.value)
            # convert input to int
            self.config[self.class_name]["count"] = value
            # is the provided integer less equal than 0 and thus illegal?
            if self.config[self.class_name]["count"] < (0 + self.config_defined):
                raise ValueError()
            else:
                self.preproc_cache[self.class_name]["count"] = value
        except ValueError as ve:
            raise Exception(
                f'For "{self.class_name}": Cannot convert input to integer >{"=" if not self.config_defined else ""} 0: "{self.class_widget.value}"'
            ) from ve
        self.editing = False


class PropertyForm(FillerStep1Form):
    def __init__(self, *args, class_count: int, properties: list[str], **keywords):
        """! Initializes a property form for a FillerStep1 application where the counts for different properties of a class can be filled in.
        If the corresponding class also needs its class count filled in, call that form first.
        The expected arguments are:
        @param class_name The label of the owl class to work on.
        @param properties A list of properties that need counts.
        @param isLast True iff this form is the last in the list of forms.
        @param config The config as yielded by the initial toml file and updated by the FillerStep1 application.
        @param class_count The count of the corresponding class.
        @param preprocessing_cache The preprocessing cache dict yielded by the OntologyBuilder's preprocessing.
        """
        self.properties = properties
        self.class_count = class_count
        self.property_widgets: list[PropertyWidget] = []
        super().__init__(*args, **keywords)

    def create(self):
        self.add_instructions()

        self.add_separator()

        if len(self.properties) > 0 and not self.config[self.class_name].get(
            "counts", []
        ):
            self.config[self.class_name]["counts"] = {}

        prop_name: str
        for prop_name in self.properties:
            self.property_widgets.append(
                self.add_widget_intelligent(
                    PropertyWidget,
                    count=self.class_count,
                    name=f'Property "{prop_name.upper()}" count: (length == {self.class_count})',
                    value=0,
                )
            )

            self.add_separator()

        self.add_ok_button()

    def on_ok(self):
        prop: str
        prop_widget: PropertyWidget
        for prop, prop_widget in zip(self.properties, self.property_widgets):
            try:
                # convert input to list of integers
                inputs = convert_input_list(prop_widget.value)
                # is there any property count that is < 0?
                if any(count < 0 for count in inputs):
                    raise ValueError()
                # only copy those lists that have a valid length
                # it would be nice, to let the user know at this stage
                # however, there will be an exception in the Multiplexer later on anyways
                if len(inputs) == self.class_count:
                    self.config[self.class_name]["counts"][prop] = inputs
                    if sum(inputs) == 0:
                        self.preproc_cache[self.class_name]["counts"].pop(prop)
                    else:
                        self.preproc_cache[self.class_name]["counts"][prop] = inputs
            except ValueError as ve:
                raise Exception(
                    f'For "{self.class_name}" and property "{prop}": Cannot convert input to valid list of integers >= 0: "{prop_widget.value}"'
                ) from ve
        self.editing = False


class FillerStep1(npyscreen.NPSAppManaged):
    def __init__(
        self,
        preprocessing_cache: dict,
        class_count_filler: list[str],
        property_count_filler: dict,
        config: dict,
        tomlIO: AbstractFileIO,
        filler_config_path: str,
    ):
        """! Initializes an NPSAppManaged object with the following custom parameters:
        @param preprocessing_cache The preprocessing cache dict yielded by the OntologyBuilder's preprocessing.
        @param class_count_filler List of class labes that need a class count.
        @param property_count_filler Dict of class labels -> property labels for properties that need a property count.
        @param config The config as yielded by the TOML file.
        @param tomlIO An TomlIO object that can dump the new config generated by this filler application.
        @param filler_config_path The path to the config generated by this filler application.
        """
        super().__init__()
        self.class_count_filler = class_count_filler
        self.property_count_filler = property_count_filler
        self.config = config
        # check whether a config was defined before this filler instance
        # if not, counts of 0 for classes are valid and will be interpreted as an exclusion
        self.config_defined = bool(config)
        self.preproc_cache = preprocessing_cache
        # unify all class names that are either in need of a count themselves or that have at least one property that needs a count
        self.class_names = list(
            set(self.class_count_filler).union(self.property_count_filler.keys())
        )
        # sort list so that the class names are always queried in the same order
        self.class_names.sort()
        self.tomlIO = tomlIO
        self.filler_config_path = filler_config_path

    def onStart(self):
        """! This method initializes the needs_widget dict and also puts empty dicts for any class name,
        that has count(s) to fill in for and does not appear in the config directly.
        The needs_widget dict serves as a look-up table for the different forms needed for each class
        as the next form is dynamically created once the previous form is finished.
        This is necessary as the property forms need the information from the class forms to
        display the needed length correctly.
        """
        self.needs_widgets = {}
        i: int
        class_name: str
        for i, class_name in enumerate(self.class_names):
            if class_name not in self.config:
                self.config[class_name] = {}

            needs_class_count = class_name in self.class_count_filler
            needs_property_counts = class_name in self.property_count_filler

            if i == 0:
                # we determine the form that is first called and set the corresponding boolean to false
                # we thus have a kind of look-ahead fashion in which we determine what the next form should be
                if needs_class_count:
                    self.add_class_form(class_name, 0, needs_property_counts, self.config_defined)
                    self.setNextForm(class_name + "_class")
                    # by setting this to false, the next form will be either the property counts of this class
                    # or a form for the next class in the list of class names
                    needs_class_count = False

                elif needs_property_counts:
                    self.add_property_form(class_name, 0)
                    self.setNextForm(class_name + "_props")
                    # by setting this to false, the next form will be a form for the next class in the list of class names
                    needs_property_counts = False

            self.needs_widgets[class_name] = {
                "class_count": needs_class_count,
                "property_counts": needs_property_counts,
            }

    def finishEditing(self, name: str):
        """! After activating the previous form's OK button, this method is called.
        It will create the next form based on the previous form or set the next form to None if the list
        of class names is exhausted.
        It also saves the config to the path passed over in the constructor after each form's completion.

        @param name The name of the class to which the returning form belonged.
        """
        idx: int = next(i for i, entity in enumerate(self.class_names) if entity == name)
        current_needs_prop_counts: bool = self.needs_widgets[name]["property_counts"]

        if self.config[name]["count"] <= 0:
            self.needs_widgets[name]["property_counts"] = False
            current_needs_prop_counts = False
            if not self.config.get("exclude_classes", False):
                self.config["exclude_classes"] = [name]
            else:
                self.config["exclude_classes"].append(name)
            self.config.pop(name)

        # does the current class still need property counts?
        if current_needs_prop_counts:
            self.add_property_form(name, idx)
            self.setNextForm(name + "_props")
            # after completing the previously added form, this method will then switch to the next class in the list if its not exhausted
            self.needs_widgets[name]["property_counts"] = False

        # if the list is not exhausted yet
        elif idx < len(self.class_names) - 1:
            idx = idx + 1
            # get booleans for the next class to work on
            next_class = self.class_names[idx]
            next_needs_class_count = self.needs_widgets[next_class]["class_count"]
            next_needs_property_counts = self.needs_widgets[next_class][
                "property_counts"
            ]

            if next_needs_class_count:
                self.add_class_form(next_class, idx, next_needs_property_counts, self.config_defined)
                self.setNextForm(next_class + "_class")
                # after completing the previously added form, this method will then check whether it needs to create a PropertyForm as well
                # otherwise, it will switch to the next class in the list if its not exhausted
                self.needs_widgets[next_class]["class_count"] = False
            elif next_needs_property_counts:
                self.add_property_form(next_class, idx)
                self.setNextForm(next_class + "_props")
                # after completing the previously added form, it will switch to the next class in the list if its not exhausted
                self.needs_widgets[next_class]["property_counts"] = False
        else:
            # list is exhausted, no next form
            self.setNextForm(None)

        # save config to file so that the user can continue when an error occurs
        self.tomlIO.dump(self.config, self.filler_config_path)

    def add_class_form(self, class_name: str, i: int, needs_property_counts: bool, config_defined: bool):
        """! Helper method that creates a ClassForm based on the parameters.

        @param class_name The name of the class that is being worked on.
        @param i The index of class_name in the list of class names. Used in determining whether this form is the last.
        @param needs_property_counts True if a PropertyForm follows this form, thus our form cannot be the last.
        @param config_defined A bool whether or not a config had been defined previous to this filler instance.
        """
        self.addForm(
            class_name + "_class",
            ClassForm,
            class_name=class_name,
            name=f"Edit {class_name}",
            config=self.config,
            preprocessing_cache=self.preproc_cache,
            config_defined=config_defined,
        )

    def add_property_form(self, class_name: str, i: int):
        """! Helper method that creates a PropertyForm based on the parameters.

        @param class_name The name of the class that is being worked on.
        @param i The index of class_name in the list of class names. Used in determining whether this form is the last.
        """
        self.addForm(
            class_name + "_props",
            PropertyForm,
            class_name=class_name,
            properties=self.property_count_filler[class_name],
            isLast=(i + 1 == len(self.class_names)),
            name=f"Edit {class_name}'s properties",
            config=self.config,
            preprocessing_cache=self.preproc_cache,
            class_count=self.preproc_cache[class_name]["count"],
        )


def convert_input_list(input:str) -> list[int]:
    """! Helper method that converts a string of ints separated by ',' into a list of ints.

    @param input The string to convert.
    @return The list of integers represented by the string.
    """
    return list(map(int, input.split(",")))
