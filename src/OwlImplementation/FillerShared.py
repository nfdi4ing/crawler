import npyscreen
import curses
from enum import IntEnum

class FillerWidget(npyscreen.TitleText):
    def __init__(self, *args, **keywords):
        super().__init__(*args, **keywords)
        # set safe_to_exit since this stupid library does
        # not actually set it for every type of widget that is editable
        self.entry_widget.safe_to_exit = self.safe_to_exit

    def show_brief_message(self, message: str, time: int =1200):
        # just copied from a widget in the library
        curses.beep()
        keep_for_a_moment = self.value
        self.value = message
        self.editing = False
        self.display()
        curses.napms(time)
        self.editing = True
        self.value = keep_for_a_moment


class FillerMultiPage(npyscreen.FormMultiPage):
    class OK_BUTTON_OFFSET(IntEnum):
        x = 6
        y = 2
    
    class CANCEL_BUTTON_OFFSET(IntEnum):
        x = 12
        y = 2

    def add_instructions(self) -> npyscreen.FixedText:
        return self.add_text(f"Controls: Moving: ARROW KEYS, Selecting: ENTER, Deselecting: CTRL+U, Delete: BACKSPACE")

    def add_separator(self) -> npyscreen.FixedText:
        return self.add_widget_intelligent(
            npyscreen.FixedText,
            value=("-" * self.columns),
            editable=False,
        )

    def add_text(self, text) -> npyscreen.FixedText:
        return self.add_widget_intelligent(
            npyscreen.FixedText,
            value=text,
            editable=False,
        )

    def add_ok_button(
        self,
        text: str = "OK",
        offset_x: int = OK_BUTTON_OFFSET.x,
        offset_y: int = OK_BUTTON_OFFSET.y,
    ):
        # Create an ok button on the bottom right
        my, mx = self.curses_pad.getmaxyx()
        ok_button_text = text
        my -= offset_y
        mx -= offset_x + len(ok_button_text)
        self.ok_button = self.add_widget(
            npyscreen.ButtonPress,
            name=ok_button_text,
            rely=my,
            relx=mx,
            use_max_space=True,
            when_pressed_function=self.on_ok,
        )

    def add_cancel_button(
        self,
        text: str = "CANCEL",
        offset_x: int = CANCEL_BUTTON_OFFSET.x,
        offset_y: int = CANCEL_BUTTON_OFFSET.y,
    ):
        c_button_text = text
        cmy, cmx = self.curses_pad.getmaxyx()
        cmy -= offset_y
        cmx -= offset_x + len(c_button_text)
        self.c_button = self.add_widget(
            npyscreen.ButtonPress,
            name=c_button_text,
            rely=cmy,
            relx=cmx,
            use_max_space=True,
            when_pressed_function=self.on_cancel,
        )

    def add_buttons(self, ok_text: str = "OK", cancel_text: str = "CANCEL", mirrored: bool = False):
        if mirrored:
            # ok button left of cancel button
            self.add_ok_button(ok_text, offset_x=self.CANCEL_BUTTON_OFFSET.x, offset_y=self.CANCEL_BUTTON_OFFSET.y)
            self.add_cancel_button(cancel_text, offset_x=self.OK_BUTTON_OFFSET.x, offset_y=self.OK_BUTTON_OFFSET.y)
        else:
            # cancel button left of ok button
            self.add_cancel_button(cancel_text)
            self.add_ok_button(ok_text)

    def on_ok(self):
        self.canceled = False
        self.editing = False

    def on_cancel(self):
        self.canceled = True
        self.editing = False
