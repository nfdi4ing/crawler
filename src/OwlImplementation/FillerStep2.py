from src.UtilityClasses.FileIOWrappers import AbstractFileIO, TomlIO

import npyscreen
import curses
from functools import reduce
from src.UtilityClasses.Parser import AbstractParser
from src.UtilityClasses.Postprocessor import AbstractPostprocessor
from src.OwlImplementation.FillerShared import FillerMultiPage, FillerWidget
from src.OntologyObjects.utils import get_class_level_keys, get_property_level_keys, dimensionality_of_list

TYPE_OPTIONS = [cls.__name__ for cls in AbstractParser.__subclasses__()]
# maps parser class names to indices
TYPE_INDICES = {n: i for i, n in enumerate(TYPE_OPTIONS)}

POST_PROCESSOR_OPTIONS = [
    cls.__name__ for cls in AbstractPostprocessor.__subclasses__()
]
# maps postprocessor class names to indices
POST_PROCESSOR_INDICES = {n: i for i, n in enumerate(POST_PROCESSOR_OPTIONS)}


class FillerStep2MultiSelectionWidget(npyscreen.TitleMultiSelect):
    """! This class extends npyscreen.TitleMultiSelect and overwrites the safe_to_exit() method used 
    for the selection of postprocessors(s) and type(s).
    """
    def __init__(self, *args, index: int, values_from_config: dict, indices_map: dict, **keywords):
        """!
        @param index The index of this particular widget for the value_selected list.
        @param indices Dictionary mapping class names to their selection indices. Used to reselect previously saved config selections.
        """
        self.index = index
        keywords["value"] = [indices_map[v] for v in values_from_config[self.index]]
        super().__init__(*args, **keywords)


class FillerStep2AddPathButton(npyscreen.ButtonPress):
    """! This class extends npyscreen.ButtonPress and implements a corresponding whenPressed() method.
    """
    def __init__(self, *args, index: int, parent_form: str, **keywords):
        """!
        @param parent_form The string name of the parent form of this widget used to retreive said parent from the parent app.
        @param index The index of the corresponding text field in the parent forms input_widgets list
        """
        self.index = index
        self.parent_form = parent_form
        super().__init__(*args, **keywords)

    def whenPressed(self):
        """! This method opens a file selection dialogue and appends the path of the selected file to the button's associated text field.
        The parent form's display is then refreshed.
        """
        selected_file = npyscreen.selectFile(must_exist=True, confirm_if_exists=False)
        if selected_file:
            pf = self.parent.parentApp.getForm(self.parent_form)
            pf.input_widgets[self.index].value += f"{selected_file}\n"
            pf.display()


class FillerStep2ChoiceWidget(npyscreen.TitleSelectOne):
    """! This class is a wrapper for npyscreen.TitleSelectOne and sets the initially selected value to the first selection,
    the maximum height, without which this widget is simply infinitly high, is set according to the length of the passed values.
    scroll_exit is set to True as well, as it otherwise becomes unusable.
    """
    def __init__(self, *args, **keywords):
        keywords["value"] = [0]
        keywords["max_height"] = max(3, len(keywords["values"]) + 1)
        keywords["scroll_exit"] = True
        super().__init__(*args, **keywords)


class FillerStep2TreeData(npyscreen.TreeData):
    """! This class extends npyscreen.TreeData and only modifies the get_content_for_display() method.
    In the future, more stuff could be done here to deliver a better user experience for long tree children.
    """

    def __init__(self, *args, plain_values:bool, **keywords):
        """!
        @param plain_values Determines how the content in the node is to be displayed. Currently unused.
        """
        self.plain_values = plain_values
        super().__init__(*args, **keywords)

    def get_content_for_display(self):
        # in here, values could be displayed  
        # in a certain kind but there is no 
        # actual need to yet
        if self.plain_values:
            return self.get_content().upper()
        return self.get_content().upper()

class FillerStep2LevelFiller(FillerMultiPage):
    """! This form class does the bulk lifting for this entire application. 
    It is designed to work on both class data and property data.
    """
    def __init__(self, *args, preprocessing_cache: dict, config: dict, class_name: str, class_key: str, class_count: int, depth: int, prop_name: str | None = None, prop_count: list[int] | None = None, **keywords):
        """!
        @param class_name Name of the class to work on. Used in various circumstances such as config or preprocessing cache access.
        @param preprocessing_cache Dict yielded by the OntologyMultiplexers preprocessing. This dict gets changed throughout this form.
        @param config The config dict yielded by the input toml. This dict gets changed throughout this form.
        @param class_count The __count__ attribute of the respective class.
        @param class_key Key from either get_class_level_keys() or get_property_level_keys()
        @param depth The level to work on (0 = class-wide, 1 = class-instance, 2 = property-instance)
        OPTIONAL PARAMETERS THAT HAVE TO BE SET WHEN WORKING ON PROPERTY DATA:
        @param prop_name String name of the property to work on.
        @param prop_count List of property counts. 
        """
        self.class_name = class_name
        self.preprocessing_cache = preprocessing_cache
        self.config = config
        self.class_count = class_count
        self.prop_count = prop_count
        self.prop_name = prop_name
        self.class_key = class_key
        self.depth =depth
        self.class_wide = bool(self.depth == 0)
        self.prop_instance = False
        if self.depth == 2:
            self.prop_instance = True
        super().__init__(*args, **keywords)

    def add_multiline_widget(self, i: int, j: int, parameters: dict, vertical_offset: int = 0) -> int:
        """! This method adds a npyscreen.MultiLineEdit widget to the form. Used for path(s) and pattern(s).
        @param i The index on the class level. On class-wide 0
        @param j The index on the property level. On class-instance 0
        @param parameters Dict filled with parameters to pass to the widget.
        @param vertical_offset The vertical_offset of the previous widget. 0 for first widget.

        @return vertical_offset of this widget, which is parameters["max_height"] + 2
        """
        if i == 0 and j == 0:
            # set initial offset to 4 (top bar, instructions, separator, inst_name)
            vertical_offset = 5
        if i != 0 or (i == 0 and j != 0):
            # every consecutive offset has to be increased by the height of the text box + button + inst_name
            vertical_offset = vertical_offset + parameters["max_height"] + 2
        self.input_widgets.append(
            self.add(
                npyscreen.MultiLineEdit,
                value="\n".join(parameters["values_from_config"][i] if not self.prop_instance else parameters["values_from_config"][i][j])+"\n",
                rely=vertical_offset,
                **parameters,
            )
        )
        return vertical_offset

    def purge_from_config(self):
        """! If the users input lead to an empty list => purge that subkey from the config and preprocessing cache.
        """
        # is the class name even in the config?
        if self.class_name in self.config:
            # old values must be purged from config
            # and preprocessing cache
            # is the subkey in the config?
            if self.class_key in self.config[self.class_name]:
                # is the subkey on property level?
                if self.class_key in get_property_level_keys():
                    # delete all property level data
                    self.config[self.class_name][self.class_key].pop(self.prop_name)
                    self.preprocessing_cache[self.class_name][self.class_key][self.prop_name] = []
                    # is the whole subkey dict now empty?
                    if not self.config[self.class_name][self.class_key]:
                        self.config[self.class_name].pop(self.class_key)
                else:
                    # subkey is on class level
                    self.config[self.class_name].pop(self.class_key)
                    self.preprocessing_cache[self.class_name][
                        self.class_key
                    ] = []
            # is the whole class dict now empty?
            if not self.config[self.class_name]:
                self.config.pop(self.class_name)

    def add_to_config(self, new_values:list):
        """! Every input that is not fully empty has to be added to the config.
        @param new_values The new values to save in the config.
        """
        if not self.class_name in self.config:
            # class name not present in the config already
            self.config[self.class_name] = {}
        # add new values to config and overwrite preprocessing cache
        # as new values are now directly defined in config for this class
        if self.class_key in get_property_level_keys():
            if not self.class_key in self.config[self.class_name]:
                self.config[self.class_name][self.class_key] = {}
                # define property on this class. Inherited values now get saved into this class as well and become irrelevant.
            self.config[self.class_name][self.class_key][self.prop_name] = new_values
            # set preprocessing cache entry to own class name as the property is now defined on this class
            self.preprocessing_cache[self.class_name][self.class_key][self.prop_name] = [
                self.class_name
            ]
        else:    
            self.config[self.class_name][self.class_key] = new_values
            self.preprocessing_cache[self.class_name][self.class_key] = [
                self.class_name
            ]


    def afterEditing(self):
        """! This method is called once self.editing is false I think. 
        In any case, its the last method called after all widgets have been completed successfully.
        """
        if not self.canceled:
            # only undertake any changes when form has not been canceled
            new_values = None
            if self.class_wide:
                # retreive class-wide values for each type of subkey
                if self.class_key in ["path", "paths", "pattern", "patterns"]:
                    new_values = list(filter(None, self.input_widgets[0].value.splitlines()))
                else:
                    new_values = self.input_widgets[0].get_selected_objects() or []
            elif not self.prop_instance:
                if self.class_key in ["path", "paths", "pattern", "patterns"]:
                    new_values = [list(filter(None, w.value.splitlines())) for w in self.input_widgets]
                else:
                    # the get_selected_objects() methods either all return None or all return a list
                    # as the on_ok guarantees this
                    new_values = [w.get_selected_objects() or [] for w in self.input_widgets]
            else:
                new_values = [[] for _ in range(self.class_count)]
                offset = 0
                for i in range(self.class_count):
                    if self.class_key in ["paths", "patterns"]:
                        new_values[i] = [list(filter(None, w.value.splitlines())) for w in self.input_widgets[offset:offset+self.prop_count[i]]]
                    else:    
                        new_values[i] = [w.get_selected_objects() or [] for w in self.input_widgets[offset:offset+self.prop_count[i]]]
                    offset += self.prop_count[i]

            if self.class_wide:
                # check whether new_values is non-empty
                if new_values:
                    self.add_to_config(new_values)
                else:
                    self.purge_from_config()
            elif not self.prop_instance:
                # check whether any entry in new_values is non-empty
                if any(v for v in new_values):
                    self.add_to_config(new_values)
                else:
                    self.purge_from_config()
            else:
                # check whether any entry in any entry in new_values is non-empty
                if any(any(w for w in v) for v in new_values):
                    self.add_to_config(new_values)
                else:
                    self.purge_from_config()
        # report back to parent app with class name and the subkey that has been worked on        
        self.parentApp.finishEditing(self.class_name, self.class_key)

    def create(self):
        """! This method creates all widgets once the form is called to be displayed on screen.
        """
        self.input_widgets = []
        input_class = None
        parameters = None
        values_from_config = (
            retreive_values_config(
                self.class_name,
                self.class_key,
                self.prop_name,
                self.class_count,
                self.prop_count,
                self.depth,
                self.config,
                self.preprocessing_cache,
            )
        )
        # set parameters for each type of subkey
        if self.class_key in ["path", "paths"]:
            input_class = None
            parameters = {
                "parent_form": self.class_name + "_" + self.class_key,
                "scroll_exit": True,
                "max_height": 5,
                "values_from_config": values_from_config
            }
        elif self.class_key in ["pattern", "patterns"]:
            input_class = None  
            parameters = {
                "max_height": 5,
                "scroll_exit": True,
                "values_from_config": values_from_config,
            }
        elif self.class_key in ["type", "types"]:
            input_class = FillerStep2MultiSelectionWidget
            if self.prop_instance:
                values_from_config = flatten_list(values_from_config)

            parameters = {
                "values": TYPE_OPTIONS,
                "scroll_exit": True,
                "max_height": 10,
                "values_from_config": values_from_config,
                "indices_map": TYPE_INDICES,
            }
        elif self.class_key in ["postprocessor", "postprocessors"]:
            input_class = FillerStep2MultiSelectionWidget
            if self.prop_instance:
                values_from_config = flatten_list(values_from_config)

            parameters = {
                "values": POST_PROCESSOR_OPTIONS,
                "scroll_exit": True,
                "max_height": 10,
                "values_from_config": values_from_config,
                "indices_map": POST_PROCESSOR_INDICES,
            }
        else:
            raise ValueError(
                f"Unexpected subkey for FillerStep2LevelFiller: {self.class_key}"
            )
        # Every form has the instructions and the separator. This is a design decision and not necessary for functioning. 
        # Although without them, the offsets for the multiline widgets for path(s) and pattern(s) are obviously too large.
        self.add_instructions()
        self.add_separator()
        # vertical offset for multiline widgets
        vertical_offset = 0
        # execute loop once if work is to be done on class-wide level. Otherwise execute it class count times.
        for i in range(1 if self.class_wide else self.class_count):
            # execute loop once if work is to be done not on a property-instance level. Otherwise execute it prop_count[i] times.
            for j in range(1 if not self.prop_instance else self.prop_count[i]):
                # The name of each instance for which stuff is to be added. Gets dynamically longer the deeper the level goes.
                inst_name = f"Please add {self.class_key.upper()} {(f'for class instance {i+1}')*(not self.class_wide)}{(f' and property instance {j+1}')*self.prop_instance}:"
                # add widgets for each type of subkey
                if self.class_key in ["path", "paths"]:
                    self.add_text(inst_name)
                    vertical_offset = self.add_multiline_widget(i,j,parameters, vertical_offset)
                    self.add_widget_intelligent(
                        FillerStep2AddPathButton, name="ADD PATH", index=(i if not self.prop_instance else sum(self.prop_count[:i])+j), **parameters
                    ) 
                elif self.class_key in ["pattern", "patterns"]:
                    self.add_text(inst_name)
                    vertical_offset = self.add_multiline_widget(i,j, parameters, vertical_offset)
                    self.add_separator()
                else:
                    self.input_widgets.append(
                        self.add_widget_intelligent(
                            input_class,
                            name=inst_name,
                            index=(i if not self.prop_instance else sum(self.prop_count[:i])+j),
                            **parameters,
                        )
                    )
        # add ok and cancel button
        self.add_buttons()  

class FillerStep2LevelFillerChoice(FillerMultiPage):
    """! This form asks the user whether they want to edit the data on a 
    - class-wide,
    - class-instance,
    - property-instance level.
    Depending on how the data is already present in the config, levels can be blocked in the order:
    class-wide < class-instance < property-instance.
    So once data is present on a class-instance level, it can only be edited on a class-instance level or property-instance level.
    This class on exit creates the next form of type FillerStep2LevelFiller.
    """
    def __init__(self, *args, preprocessing_cache: dict, config: dict, class_name: str, class_key: str, class_count: str, prop_name: str | None = None, prop_count: list[int] | None = None, simple_interactive: bool = False, **keywords):
        """!
        @param class_name Name of the class to work on. Used in various circumstances such as config or preprocessing cache access.
        @param preprocessing_cache Dict yielded by the OntologyMultiplexers preprocessing. This dict gets changed throughout this form.
        @param config The config dict yielded by the input toml. This dict gets changed throughout this form.
        @param class_count The __count__ attribute of the respective class.
        @param class_key Key from either get_class_level_keys() or get_property_level_keys()
        OPTIONAL PARAMETERS THAT HAVE TO BE SET WHEN WORKING ON PROPERTY DATA:
        @param prop_name String name of the property to work on.
        @param prop_count List of property counts. 
        @param simple_interactive If true, only property level will be selectable.
        """
        self.class_name =  class_name
        self.preprocessing_cache = preprocessing_cache
        self.config = config
        self.class_count = class_count
        self.prop_count = prop_count
        self.prop_name = prop_name
        self.class_key = class_key
        self.simple_interactive = simple_interactive
        super().__init__(*args, **keywords)

    def afterEditing(self):
        """! After the user has made a selection, the FillerStep2LevelFiller form is created.
        So this method does not return to the parent apps finishEditing, but rather
        sets the next form.
        """
        if self.canceled:
            self.parentApp.finishEditing(self.class_name, self.class_key)
        else:
            selection = self.choice_widget.value[0]
            # has the selection choice been limited?
            if len(self.choice_widget.values) < 3 and self.prop_name:
                # correct for old indices
                selection += 1
            if self.simple_interactive:
                selection = 2
            form_name = self.class_name + "_" + self.class_key
            self.parentApp.addForm(
                form_name,
                FillerStep2LevelFiller,
                class_name=self.class_name,
                prop_name=self.prop_name,
                prop_count=self.prop_count,
                preprocessing_cache=self.preprocessing_cache,
                depth=selection,
                class_count=self.class_count,
                class_key=self.class_key,
                config=self.config,
                name=f"EDIT {self.class_name.upper() if not self.prop_name else self.prop_name.upper()}'S {self.class_key.upper()}",
            )
            self.parentApp.setNextForm(form_name)

    def create(self):
        self.add_instructions()
        self.add_separator()
        first_values = ["CLASS-WIDE level", "CLASS-INSTANCE level"] if not self.simple_interactive else []
        self.choice_widget = self.add_widget_intelligent(
            FillerStep2ChoiceWidget,
            name=f'Do you want to edit {self.class_key.upper()} for class {self.class_name.upper()}{f" and property {self.prop_name.upper()}" if self.prop_name else ""} on a ',
            values=first_values + ["PROPERTY-INSTANCE level"]*bool(self.prop_name),
        )
        self.add_buttons()
        if not self.simple_interactive:
            if self.class_key in get_class_level_keys():
                # this entire form can be skipped if any class in the preprocessing cache
                # has a list of dimensionality 2 (=> class-instance specific) in the config
                for c in self.preprocessing_cache[self.class_name][self.class_key]:
                    if dimensionality_of_list(self.config[c][self.class_key]) == 2:
                        self.choice_widget.value = [1]
                        self.choice_widget.editable = False
                        break
            else:
                for c in self.preprocessing_cache[self.class_name][self.class_key][self.prop_name]:
                    dim = dimensionality_of_list(self.config[c][self.class_key][self.prop_name])
                    if dim >= 2:
                        # limit selection 
                        self.choice_widget.value = [0]
                        self.choice_widget.values = ["CLASS-INSTANCE level", "PROPERTY-INSTANCE level"]
                    if dim == 3:
                        self.choice_widget.value = [1]
                        self.choice_widget.editable = False
                        break

class FillerStep2DisplayForm(FillerMultiPage):
    """! This form displays class and property data in a tree fashion. 
    It works, but not really well. This could be improved in future work.
    """
    def __init__(self, *args, preprocessing_cache: dict, config: dict, class_name: str, prop_name: str | None = None, **keywords):
        """!
        @param class_name Name of the class to work on. Used in various circumstances such as config or preprocessing cache access.
        @param preprocessing_cache Dict yielded by the OntologyMultiplexers preprocessing. This dict gets changed throughout this form.
        @param config The config dict yielded by the input toml. This dict gets changed throughout this form.
        OPTIONAL PARAMETERS THAT HAVE TO BE SET WHEN WORKING ON PROPERTY DATA:
        @param prop_name String name of the property to work on.
        """
        self.class_name = class_name
        self.prop_name = prop_name
        self.preprocessing_cache = preprocessing_cache
        self.config = config
        super().__init__(*args, **keywords)

    def afterEditing(self):
        """! This method returns to the parent app depending on whether the form was completed successfully or not.
        In case of successfull completion, send selected values to work on back to parent app.
        Otherwise, send "RETURN_MAIN" and thus instruct the parent app to return to the main form.
        """
        if self.canceled:
            self.parentApp.finishEditing("FillerStep2MainForm", self.class_name)
        else:
            if len(self.selected_values) == 0:
                if self.prop_name:
                    self.parentApp.finishEditing(self.class_name, self.prop_name)
                else:
                    self.parentApp.finishEditing(self.class_name, self.class_name)
            else:
                self.parentApp.finishEditing(self.class_name, self.selected_values)


    def create(self):
        self.add_instructions()
        self.add_separator()
        class_root = FillerStep2TreeData(
            content="CRAWLER ATTRIBUTES",
            selectable=False,
            ignore_root=False,
            plain_values=False,
        )
        if not self.prop_name:
            # for each subkey on class data retreive from the preprocessing cache the corresponding config data
            # to display to the user in a tree fashion
            for k in get_class_level_keys():
                class_wg = class_root.new_child(
                    content=k, selectable=True, plain_values=False
                )
                v = []
                for parent_pc in self.preprocessing_cache[self.class_name][k]:
                    v += self.config[parent_pc][k]
                class_wg.new_child(content=str(v), selectable=False, plain_values=True)
        else:
            for k in get_property_level_keys():
                class_wg = class_root.new_child(
                    content=k, selectable=True, plain_values=False
                )
                v = []
                for parent_pc in self.preprocessing_cache[self.class_name][k][self.prop_name]:
                    v += self.config[parent_pc][k][self.prop_name]
                class_wg.new_child(content=str(v), selectable=False, plain_values=True)

        self.class_tree = self.add_widget_intelligent(
            npyscreen.MLTreeMultiSelect,
            values=class_root,
            scroll_exit=True,
        )
        self.add_buttons(ok_text="EDIT", cancel_text="BACK")

    def on_ok(self):
        """! This method collects the selected values from the tree widget and saves them. 
        It then calls the method on the super class
        """
        self.selected_values = list(
            self.class_tree.get_selected_objects(return_node=False)
        )
        super().on_ok()

class FillerStep2ClassPropChoice(FillerMultiPage):
    """! This form lets the user choose between editing the class or any of its properties.
    """
    def __init__(self, *args, class_name: str, properties: list[str], simple_interactive: bool = False, **keywords):
        """!
        @param class_name Name of the class to work on.
        @param properties List of properties of the class to work on.
        @param simple_interactive If true, only properties are available for selection.
        """
        self.class_name = class_name
        self.properties = properties
        self.simple_interactive = simple_interactive
        super().__init__(*args, **keywords)

    def create(self):
        self.add_instructions()
        first_val = ["CLASS " + self.class_name] if not self.simple_interactive else []
        self.choice_widget = self.add_widget_intelligent(
            npyscreen.TitleSelectOne,
            name="What do you want to edit?",
            value=[0],
            values=first_val+self.properties,
            max_height=10,
            scroll_exit= True,
        )
        self.add_buttons(cancel_text="BACK")

    def afterEditing(self):
        """! This method reports back to the parent app according to the selection that was made.
        """
        if self.canceled:
            self.parentApp.finishEditing(self.class_name, "RETURN_MAIN")
        else:
            selection = self.choice_widget.value[0]
            if not self.simple_interactive:
                if selection == 0:
                    self.parentApp.finishEditing(self.class_name, self.class_name)
                else:
                    self.parentApp.finishEditing(self.class_name, self.properties[selection-1])
            else:
                self.parentApp.finishEditing(self.class_name, self.properties[selection])

class FillerStep2MainForm(FillerMultiPage):
    """! This form is the applications main form, although it conveys the least information.
    It simply shows a list selection of classes that can be edited and two buttons, one to edit the selected class 
    and one to close the application and continue with the crawler.
    """

    def __init__(self, *args, class_names:list[str], **keywords):
        """!
        @param class_names List of names of the different classes that can be edited.
        """
        self.class_names = class_names
        super().__init__(*args, **keywords)

    def afterEditing(self):
        """! This method reports back to the parent app depending on the button that has been pressed.
        If the "EDIT" button has been pressed, the class name that has been selected is sent back.
        Otherwise, "FINISHED" is sent.
        """
        if self.canceled:
            self.parentApp.finishEditing("FINISHED", "")
        else:
            self.parentApp.finishEditing(
                "FillerStep2MainForm", self.choice_widget.get_value()[0]
            )

    def create(self):
        self.add_instructions()
        self.add_separator()

        self.choice_widget = self.add_widget_intelligent(
            npyscreen.TitleSelectOne,
            name="Please choose a class to edit:",
            values=self.class_names,
            value=[
                0,
            ],
            max_height=10,
            scroll_exit=True,
        )

        self.add_buttons(ok_text="EDIT", cancel_text="DONE", mirrored=True)

class FillerStep2(npyscreen.NPSAppManaged):
    """! This class extends npyscreen.NPSAppManaged and is the application contained in this file.
    """
    def __init__(
        self,
        preprocessing_cache: dict,
        data_dict: dict,
        config: dict,
        tomlIO: AbstractFileIO,
        filler_config_path: str,
        simple_interactive: bool
    ):
        """!
        @param preprocessing_cache Dict yielded by the OntologyMultiplexers preprocessing. This dict gets changed throughout this form.
        @param data_dict The dictionary yieled by step 1 of the crawler.
        @param config The config dict yielded by the input toml. This dict gets changed throughout this form.
        @param tomlIO TomlIO object that is used to save the config to disk.
        @param filler_config_path Path where config is saved to disk at.
        @param simple_interactive Simplifies the choices offered by this filler application to only property (level) choices.
        """
        super().__init__()
        self.preprocessing_cache = preprocessing_cache
        self.config = config
        self.tomlIO = tomlIO
        self.filler_config_path = filler_config_path
        self.data_dict = data_dict
        self.simple_interactive = simple_interactive

        self.class_names = list(data_dict.keys())
        self.class_names.sort()

        # these dicts always only contain at most one key 
        # and are used as a sort of queue
        self.class_edit_lookup = {}
        self.prop_edit_lookup = {}

    def onStart(self):
        self.add_main_form()

    def finishEditing(self, form_name: str, status: int | str | list[str] | None = None):
        """! This method does most of the management of the different forms. 
        @param form_name The name of the form that just finished. Most of the time, its a class name for which edits have been done.
        @param status A status that can be optionally passed as well. Most of the time, its a subkey from get_class_level_keys() or get_property_level_keys().
        """
        if form_name == "FillerStep2MainForm":
            # The main form has just returned and a class is to be edited
            if isinstance(status, int):
                class_name = self.class_names[status]
            elif isinstance(status, str):
                class_name = status
            else:
                raise ValueError(f"Illegal status passed to finishEditing()! {status}")
            form_name = class_name+"_"+"classprop"
            self.addForm(
                form_name,
                FillerStep2ClassPropChoice,
                class_name=class_name,
                properties=[p for p in sorted(list(self.data_dict[class_name])) if p not in ["__count__", "__restrictions__", "__is_subclass_of__"]],
                name=f"OPTIONS FOR EDITING {class_name.upper()}",
                simple_interactive=self.simple_interactive,
            )
            self.setNextForm(form_name)

        elif form_name == "FINISHED":
            # the main form has just returned and the application is finished
            self.setNextForm(None)

        elif form_name in self.class_names:
            # a class has to be edited
            if isinstance(status, list):
                # subkeys to edit have been selected and now have to be passed into a cache
                if all(x in get_class_level_keys() for x in status):
                    if form_name not in self.class_edit_lookup:
                        # this dummy value is necessary as we immediatly pop
                        # the first value in the cache if the status is a single subkey.
                        # So the dummy value will be popped and status set to the actual first subkey.
                        dummy_val = get_class_level_keys()[0]
                        self.class_edit_lookup[form_name] = [dummy_val] + status
                        status = dummy_val
                if all(x in get_property_level_keys() for x in status):
                    prop = list(self.prop_edit_lookup[form_name].keys())[0]
                    if not self.prop_edit_lookup[form_name][prop]:
                        dummy_val = get_property_level_keys()[0]
                        self.prop_edit_lookup[form_name][prop] = [dummy_val] + status
                        status = dummy_val

            if status in self.class_names:
                # display the data for the given class name
                self.add_class_form(status)
            elif status in self.data_dict[form_name]:
                # display the data for the given property name
                self.add_property_form(form_name, status)
            elif status in get_class_level_keys():
                # save config after every form
                self.tomlIO.dump(self.config, self.filler_config_path)
                self.class_edit_lookup[form_name].pop(0)
                if not self.class_edit_lookup[form_name]:
                    # all things queued are completed
                    self.class_edit_lookup.pop(form_name)
                    # return to class form
                    self.add_class_form(form_name)
                else:
                    next_status = self.class_edit_lookup[form_name][0]
                    class_name = form_name
                    if next_status == "postprocessor":
                        npyscreen.notify("POSTPROCESSORS ARE NOT IMPLEMENTED YET!")
                        curses.napms(2000)
                        self.finishEditing(class_name, next_status)
                    else:
                        form_name = class_name + "_yesno"
                        self.addForm(
                            form_name,
                            FillerStep2LevelFillerChoice,
                            class_name=class_name,
                            preprocessing_cache=self.preprocessing_cache,
                            config=self.config,
                            class_count=self.data_dict[class_name]["__count__"],
                            class_key=next_status,
                            name=f"EDIT {class_name.upper()}'S {next_status.upper()}",
                        )
                        self.setNextForm(form_name)

            elif status in get_property_level_keys():
                # there is only one key in the dict, so retreive that
                # this is done as a kind of workaround since
                # this section came in last and had to be adapted to the
                # already existing way of processing finished forms
                # with only the class form and a single status variable
                prop = list(self.prop_edit_lookup[form_name].keys())[0]
                self.tomlIO.dump(self.config, self.filler_config_path)
                self.prop_edit_lookup[form_name][prop].pop(0)
                if not self.prop_edit_lookup[form_name][prop]:
                    # all things queued are completed
                    self.prop_edit_lookup.pop(form_name)
                    # return to prop form
                    self.add_property_form(form_name, prop)
                else:
                    next_status = self.prop_edit_lookup[form_name][prop][0]
                    class_name = form_name
                    # TODO this can be removed once postprocessors have been implemented
                    if next_status == "postprocessors":
                        npyscreen.notify("POSTPROCESSORS ARE NOT IMPLEMENTED YET!")
                        curses.napms(2000)
                        self.finishEditing(class_name, next_status)
                    else:
                        form_name = class_name + "_yesno"
                        self.addForm(
                            form_name,
                            FillerStep2LevelFillerChoice,
                            class_name=class_name,
                            preprocessing_cache=self.preprocessing_cache,
                            config=self.config,
                            class_count=self.data_dict[class_name]["__count__"],
                            class_key=next_status,
                            prop_name=prop,
                            prop_count=self.data_dict[class_name][prop],
                            name=f"EDIT {prop.upper()}'S {next_status.upper()}",
                            simple_interactive = self.simple_interactive,
                        )
                        self.setNextForm(form_name)

            elif status == "RETURN_MAIN":
                    self.add_main_form()

    def add_main_form(self):
        """! This method adds the main form to the application or
        sets the next form to the already existing main form.
        """
        if not hasattr(self, "main_form"):
            npyscreen.notify("WARNING: DO NOT CHANGE INHERITANCE (I.E. DELETE VALUES OR INTRODUCE NEW INHERITANCE RELATIONSHIPS)!")
            curses.napms(6000)
            self.main_form = self.addForm(
                "FillerStep2MainForm",
                FillerStep2MainForm,
                class_names=self.class_names,
                name=f"MAIN",
            )
        self.main_form.switch_page(0)
        self.setNextForm("FillerStep2MainForm")
        self.class_edit_lookup = {}
        self.prop_edit_lookup = {}

    def add_class_form(self, class_name: str):
        """! This method adds a form for displaying and choosing class crawler attributes.
        """
        self.addForm(
            class_name,
            FillerStep2DisplayForm,
            class_name=class_name,
            config=self.config,
            preprocessing_cache=self.preprocessing_cache,
            name=f"EDIT {class_name}".upper(),
        )
        self.setNextForm(class_name)

    def add_property_form(self, class_name: str, prop: str):
        """! This method adds a form for displaying and choosing property crawler attributes
        """
        self.prop_edit_lookup[class_name] = {prop: []}
        self.addForm(
            class_name+"_"+prop,
            FillerStep2DisplayForm,
            class_name=class_name,
            prop_name=prop,
            config=self.config,
            preprocessing_cache=self.preprocessing_cache,
            name=f"EDIT {class_name}'s {prop}".upper(),
        )
        self.setNextForm(class_name+"_"+prop)


def retreive_values_config(
    class_name: str, subkey: str, prop_name: str, class_count: int, prop_count: list[int], depth: int, config: dict, preprocessing_cache: dict
) -> list[str]:
    if subkey in get_class_level_keys():
        return retreive_class_values_config(
            class_name, subkey, class_count, (depth==0), config, preprocessing_cache
        )
    elif subkey in get_property_level_keys():
        return retreive_prop_values_config(class_name, subkey, prop_name, class_count, prop_count, depth, config, preprocessing_cache)
    else:
        raise ValueError(f"Unrecognized key {subkey} for config!")

def retreive_prop_values_config(
    class_name, subkey, prop_name, class_count, prop_count, depth, config, preprocessing_cache
):
    if not preprocessing_cache[class_name][subkey]:
        if depth == 0:
            return [[]]
        elif depth == 1:
            return [[] for _ in range(class_count)]
        else:
            return [[[] for _ in range(prop_count[i])] for i in range(class_count)]
    else:
        if depth == 0:
            ret = []
            for c in preprocessing_cache[class_name][subkey][prop_name]:
                ret += config[c][subkey][prop_name]
            return [ret]
        elif depth == 1:
            ret = [[] for _ in range(class_count)]
            for ls in [config[c][subkey][prop_name] for c in preprocessing_cache[class_name][subkey][prop_name]]:
                if ls and isinstance(ls[0], list):
                    # if we are class-instance: elementwise append
                    ret = [a + b for a, b in zip(ret, ls)]
                else:
                    # if we found class-level elements:
                    # append that list to all elements in ret
                    ret = [a + ls for a in ret]
            return ret 
        else:
            ret = [[[] for _ in range(prop_count[i])] for i in range(class_count)]
            for ls in [config[c][subkey][prop_name] for c in preprocessing_cache[class_name][subkey][prop_name]]:
                dim = dimensionality_of_list(ls)
                if dim == 1:
                    ret = [[l2 + ls for l2 in l1] for l1 in ret]
                elif dim == 2:
                    ret =  [[l2 + ls[i] for l2 in l1] for i, l1 in enumerate(ret)]
                else:
                    ret = [[l2 + ls[i][j] for j, l2 in enumerate(l1)] for i,l1 in enumerate(ret)]
            return ret

def retreive_class_values_config(
    class_name, subkey, class_count, class_wide, config, preprocessing_cache
):
    if not preprocessing_cache[class_name][subkey]:
        if class_wide:
            return [[]]
        return [[] for _ in range(class_count)]
    if class_wide:
        # assume that class_wide can only be true
        # iff it always was true and all paths are class wide
        ret = []
        for c in preprocessing_cache[class_name][subkey]:
            ret += config[c][subkey]
        return [ret]
    else:
        # we could either now switch from class-wide
        # to class-instance or we have been class-instance
        # from the beginning
        ret = [[] for _ in range(class_count)]
        for ls in [config[c][subkey] for c in preprocessing_cache[class_name][subkey]]:
            if ls and isinstance(ls[0], list):
                # if we are class-instance: elementwise append
                ret = [a + b for a, b in zip(ret, ls)]
            else:
                # if we found class-level elements:
                # append that list to all elements in ret
                ret = [a + ls for a in ret]
        return ret

def flatten_list(ls:list[list]) -> list:
    """! This method flattens a given list of lists in the sense that all its sublists will be merged into one.
    @param ls List of lists to flatten.
    @return The flattened list. 
    """
    ret = []
    for i in range(len(ls)):
        for ll in ls[i]:
            ret.append(ll)
    return ret
