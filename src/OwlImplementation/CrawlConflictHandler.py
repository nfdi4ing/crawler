import npyscreen
import json
import re
import argparse

from src.UtilityClasses.FileIOWrappers import *

OTHER_NAME = "Other (type manually)"

class EntityForm(npyscreen.FormMultiPageAction):
    def __init__(self, *args, **keywords):
        self.entity = keywords.pop('entity')
        self.data = keywords.pop('data')
        self.menus = {}
        self.edit_data = {}
        self.edit_manual = {}

        super(EntityForm, self).__init__(*args, **keywords)

    def afterEditing(self):
        self.parentApp.finishEditing(self.entity, self.edit_data, self.edit_manual)

    def create(self):
        self.how_exited_handers[npyscreen.wgwidget.EXITED_ESCAPE]  = self.exit_application  
        for property in self.data:
            values = []
            for value in self.data[property]:
                values.append(f"\"{value['match']}\" from file {value['file']}")

            values.append(OTHER_NAME)

            self.edit_data[property] = self.add_widget_intelligent(npyscreen.TitleSelectOne, name=property, values=values, value=[0,], scroll_exit=True, max_height=4, use_two_lines=True)
            self.edit_manual[property] = self.add_widget_intelligent(npyscreen.TitleText, name=f"Other \"{property}\"", value="", hidden=True)

    def adjust_widgets(self):
        for i, widget in enumerate(self.edit_data.keys()):
            if self.edit_data[widget].get_selected_objects()[0] == OTHER_NAME:
                if self.edit_manual[widget].hidden == True:
                    self.edit_manual[widget].hidden = False   
                    self.display()           
            else :
                if self.edit_manual[widget].hidden == False:
                    self.edit_manual[widget].hidden = True
                    self.display()


    def exit_application(self):
        self.parentApp.setNextForm(None)
        self.editing = False
        self.parentApp.switchFormNow()

    

class EntityApplication(npyscreen.NPSAppManaged):
    def __init__(self, input_file, output_file):
        super(EntityApplication, self).__init__()

        with open(input_file, 'r') as json_file:
            self.data = json.load(json_file)

        self.output_io = JsonIO()
        if output_file.endswith('.yaml'):
            self.output_io = YamlIO()

        self.output_file = output_file

        # if data dictionary only has empty objects, exit the program
        if all(not bool(self.data[entity]) for entity in self.data):
            print("No conflicts found.")
            exit(0)

    def onStart(self):
       # loop through all the entities which have at lest one conflict
       valid_entities = list(filter(lambda x : len(self.data[x]) > 0, self.data.keys()))
       for i, entity in enumerate(valid_entities):           
           form_name = entity
           if i == 0:        
               form_name = "MAIN"

    
           self.addForm(form_name, EntityForm, entity=entity, data=self.data[entity], isLast=(i+1==len(valid_entities)), name=f'Edit {entity} ({i+1}/{len(valid_entities)})')
        


    def finishEditing(self, name, edit_data, edit_manual):
        # index based on lambda. If name is found, return the index
        idx = next(i for i, entity in enumerate(self.data) if entity == name)

        output_data = self.output_io.load(self.output_file)


        for el in edit_data.keys():
            value = edit_data[el].get_selected_objects()[0]

            if value == OTHER_NAME:
                value = edit_manual[el].value.strip()
            else:
                value = re.findall(r'"(.*?)"', value)[0]

            output_data[name][el] = value
            
        
        self.output_io.dump(output_data, self.output_file)

        if idx + 1 < len(self.data):
            next_key = list(self.data)[idx + 1]
            self.setNextForm(next_key)
        else:
            self.setNextForm(None)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description=""
    )
   
    parser.add_argument(
        "conflict_path",
        type=str,
        help="Path of the conflict file.",
    )

    parser.add_argument(
        "crawl_path",
        type=str,
        help="Path of the crawled dictionary file.",
    )

    args = parser.parse_args()
    conflict_path = args.conflict_path
    crawl_path = args.crawl_path


    ConflictApp = EntityApplication(conflict_path, crawl_path).run()