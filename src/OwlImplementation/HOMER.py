import argparse
from src.OntologyObjects.OntologyBuilder import OntologyBuilder
from src.OntologyObjects.OntologyExtractor import OntologyExtractor
from src.UtilityClasses.FileIOWrappers import *
from src.OntologyObjects.owlready2Wrappers.OntologyReader import OntologyReader
from src.OntologyObjects.OntologyMultiplexer import OntologyMultiplexer
from src.OntologyObjects.OntologyHandler import OntologyHandler
from src.OntologyObjects.utils import get_label

supported_output_file_extensions = ["json", "yaml"]
supported_input_file_extensions = ["owl", "json", "yaml"]

parser = argparse.ArgumentParser(
    description=""
)
parser.add_argument(
    "in_path",
    type=str,
    help="Path to either ontology OWL file or dictionary to be multiplexed.",
)
parser.add_argument(
    "out_path",
    type=str,
    help=f"Path to store the generated file.\nCurrently supported file extensions: {supported_output_file_extensions}.\nOnly the third step's output will be found under the original output path. The other steps outputs will be appended by stepX.",
)
parser.add_argument(
    "-c",
    "--config-path",
    dest="config_path",
    type=str,
    help="Path to config file in toml format.",
    nargs="?",
    default="",
)
parser.add_argument(
    "-ac",
    "--additional-config-paths",
    dest="additional_config_paths",
    type=str,
    help="Path to config files in toml format.",
    nargs="*",
    default="",
)

parser.add_argument(
    "-s",
    "--step",
    dest="step",
    type=int,
    help="The step starting from which to execute.\n1 = Start from reading ontology.\n2 = Start with multiplexing dictionary.\n3 = Start with crawling. (defaults to 1)",
    default=1,
)
parser.add_argument(
    "-l",
    "--language",
    dest="language",
    type=str,
    nargs="?",
    help="Property/Class language tag to be prioritized. (defaults to English)",
    default="en",
)
# TODO implement a reader; implement choices;
parser.add_argument(
    "-r",
    "--reader",
    dest="reader",
    type=str,
    nargs="?",
    default=None,
    help="Choose the reader for the provided ontology file.",
    choices=[None],
)
parser.add_argument(
    "--n-workers",
    type=int,
    dest="nworkers",
    default=4,
    help="Specify the number of workers to be used for the crawling process in parallel.",
    required=False,
)
parser.add_argument(
    "--ontology",
    type=str,
    dest="inheritance_ontology",
    default="",
    required=False,
    help="Ontology to be provided for inherting paths in step 2 when multiplexing.",
)
parser.add_argument(
    "--start-dir",
    dest="startdir",
    type=str,
    help="Path to the directory where the search will start. If a config is specified the path mentioned here will be the fallback if none of those in the config yielded a match.",
    required=False,
    default="",
)
parser.add_argument(
    "-i",
    "--interactive",
    dest="interactive_filler",
    help="Activate interactive filler help where 0 applies it everywhere and 1, 2 only before the respective steps.",
    type=int,
    required=False,
    default=-1,
    choices=range(3)
)
parser.add_argument(
    "--consider_only_child_classes",
    dest="consider_all_classes",
    help="By default, all classes are considered to be part of the output of step 1. Set this flag to have only those classes considered for the output of step 1, that do not have any children.",
    action="store_false"
)
parser.add_argument(
    "--simple_interactive",
    dest="simple_interactive",
    help="Activates interactive mode for step 1 and a simplified version for step 2 and conflict handling for step 3.",
    action="store_true"
)

# read in cli arguments
args = parser.parse_args()
in_path = args.in_path
out_path = args.out_path
lang = args.language
config_path = args.config_path
additional_config_paths = args.additional_config_paths
step = args.step
nworkers = args.nworkers
start_dir = args.startdir
inheritance_ontology = args.inheritance_ontology
interactive_filler = args.interactive_filler
consider_all_classes = args.consider_all_classes
simple_interactive = args.simple_interactive

# deal with possible config
config = {}
tomlIO = TomlIO()
if config_path != "":
    config = tomlIO.load(config_path)
    
if additional_config_paths != []:
    for additional_path in additional_config_paths:
        config = config | tomlIO.load(additional_path)

if config == {}:
    # this guarantees consistency in behavior for the OntologyExtractor
    config = None

# check input path file extension for problems related to the desired step to start execution from
in_path_ext = get_file_extension(
    in_path, supported_endings=supported_input_file_extensions
)
if in_path_ext == "owl" and step != 1:
    raise ValueError(
        f"The input given is an OWL file but the script is set to start executing from step {step}!"
    )
if in_path_ext != "owl" and step == 1:
    raise ValueError(
        f"The input given was not an OWL file but {in_path_ext} and the script is set to start executing from step 1!"
    )

# determine output path file extension and set output file IO accordingly
out_path_ext = get_file_extension(
    out_path, supported_endings=supported_output_file_extensions
)
output_io = JsonIO()
if out_path_ext == "yaml":
    output_io = YamlIO()

filler_config_path = out_path.rsplit(".", 1)[0] + "_config.toml"
# append the step numbers to the intermediate outputs for step 1 and 2
step_1_out_path = "_step1.".join(out_path.rsplit(".", 1))
step_2_out_path = "_step2.".join(out_path.rsplit(".", 1))
step_3_out_path = out_path

if step == 1:
    # Actually performs the ontology processing and writes out the first json
    # TODO if any reader has been implemented at some point: pass that over here
    reader = OntologyReader(lang=lang)
    ontology_dict = OntologyExtractor(reader, config=config, crawl_all_classes=consider_all_classes).extract(in_path)

    builder = OntologyBuilder()
    preprocessing_cache = {}

    if config:
        # do preprocessing on the config
        preprocessing_cache = builder.preprocessing(ontology_dict, config)
    if interactive_filler in [0,1] or simple_interactive: 
        preprocessing_cache = builder.preprocessing(ontology_dict, config)
        # if we use interactive fillers: determine crawl properties that have to be filled in
        count_filler = [owl_class for owl_class in preprocessing_cache if preprocessing_cache[owl_class]["count"] == -1]
        counts_filler = {}
        for owl_class in ontology_dict:
            class_label = get_label(owl_class)
            counts_filler[class_label] = [prop for prop in preprocessing_cache[class_label]["counts"] if preprocessing_cache[class_label]["counts"][prop] == [-1]]
            if not counts_filler[class_label]:
                # if no counts have to be filled in: pop that class from the dict
                counts_filler.pop(class_label)

        if not count_filler and not counts_filler:
            print("Nothing left to fill in for step 1!")
        else:
            print("Starting filler for step 1!")
            print(f"Need to fill for class counts: {count_filler}")
            print(f"Need to fill for property counts: {counts_filler}")
            from src.OwlImplementation.FillerStep1 import FillerStep1
            rerun_extractor = False
            if config == None:
                # this needs to be done to actually keep results from FillerStep1
                config = {}
                # an include filter is added in the filler, so run the extractor again for consistent builder
                rerun_extractor = True

            FillerStep1(preprocessing_cache, count_filler, counts_filler, config, tomlIO, filler_config_path).run()
            
            if rerun_extractor:
                ontology_dict = OntologyExtractor(reader, config=config, crawl_all_classes=consider_all_classes).extract(in_path)

    built_dict = builder.build(ontology_dict, preprocessing_cache)
    output_io.dump(built_dict, step_1_out_path)

    inheritance_ontology = in_path

# this input_io is only needed for steps not starting with an ontology
input_io = JsonIO()
if in_path_ext == "yaml":
    input_io = YamlIO()

if step < 3:
    input_data = None
    if step == 1:
        # have we executed step 1 ourselves?
        in_path = step_1_out_path
        input_io = output_io
    
    label_class_dict = {}
    if inheritance_ontology:
        loaded_onto = (
            OntologyReader(lang=lang).get_ontology(inheritance_ontology).load()
        )
        for owl_class in loaded_onto.classes():
            label_class_dict[get_label(owl_class)] = owl_class

    input_data = input_io.load(in_path)
    preprocessing_cache = {}
    preprocessing_cache = OntologyMultiplexer.preprocessing(input_data, config, label_class_dict)
    if interactive_filler in [0,2] or simple_interactive:
        from src.OwlImplementation.FillerStep2 import FillerStep2
        print("Filler for step 2")
        if config == None:
            config = {}
        FillerStep2(preprocessing_cache, input_data, config, tomlIO, filler_config_path, simple_interactive).run()
    
        # these two lines are only sometimes necessary as there is a bug
        # where the config and preprocessing_cache are reset after FillerStep2
        # which does not make sense after 2 hours of bug hunting.
        # The bug appears when using the --simple_interactive flag and no config.
        # But for just --interactive 2, everything is fine here. 
        config = tomlIO.load(filler_config_path)
        preprocessing_cache = OntologyMultiplexer.preprocessing(input_data, config, label_class_dict)

    multiplexed_data = OntologyMultiplexer.multiply(input_data, preprocessing_cache, config)
    output_io.dump(multiplexed_data, step_2_out_path)

    if not config:
        raise ValueError("No config was provided, so no paths, patterns nor types are known! Please provide one using -c/--config!")
    in_path = step_2_out_path
    input_io = output_io

handler = OntologyHandler(data=input_io.load(in_path), start_directory=start_dir,crawl_workers=nworkers,config=config)

# perform the crawling as defined inside the data
filled_ontology, save_state = handler.handle()

output_io.dump(filled_ontology, out_path)

# save conflict handling state always as json
prefix = out_path.rsplit(".", 1)[0]
JsonIO().dump(save_state, f"{prefix}_conflict.json")

if simple_interactive:
    print("Conflict handling for step 3")
    from src.OwlImplementation.CrawlConflictHandler import EntityApplication
    ConflictApp = EntityApplication("_conflict.".join(out_path.rsplit(".", 1)), out_path).run()