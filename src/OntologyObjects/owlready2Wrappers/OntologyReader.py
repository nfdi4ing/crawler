from abc import ABC, abstractmethod
from owlready2 import get_ontology
from src.OntologyObjects.owlready2Wrappers.Ontology import Ontology, AbstractOntology

class AbstractOntologyReader(ABC):

    @abstractmethod
    def get_ontology(self, base_iri: str) -> AbstractOntology:
        raise NotImplementedError

class OntologyReader(AbstractOntologyReader):

    def __init__(self, lang: str = 'en'):
        self.lang = lang

    def get_ontology(self, base_iri):
        return Ontology(get_ontology('file://' + base_iri), lang=self.lang)