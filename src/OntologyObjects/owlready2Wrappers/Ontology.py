from abc import ABC, abstractmethod
from src.OntologyObjects.owlready2Wrappers.OntologyClass import AbstractOntologyClass, OntologyClass
from src.OntologyObjects.owlready2Wrappers.OntologyProperty import AbstractOntologyProperty, OntologyProperty
from owlready2 import Ontology as OWLOntology

class AbstractOntology(ABC):

    @abstractmethod
    def classes(self) -> list[AbstractOntologyClass]:
        raise NotImplementedError

    @abstractmethod
    def properties(self) -> list[AbstractOntologyProperty]:
        raise NotImplementedError

    @abstractmethod
    def load(self) -> 'AbstractOntology':
        raise NotImplementedError

class Ontology(AbstractOntology):

    def __init__(self, onto: OWLOntology, lang: str = 'en'):
        self.onto = onto
        self.lang = lang

    def classes(self) -> list[AbstractOntologyClass]:
        return [OntologyClass(x, self.lang) for x in self.onto.classes()]

    def properties(self) -> list[AbstractOntologyProperty]:
        return [OntologyProperty(x, self.lang) for x in self.onto.properties()]

    def load(self) -> 'AbstractOntology':
        return Ontology(self.onto.load(), lang=self.lang)

    def __hash__(self) -> int:
        return hash(self.onto)

    def __eq__(self, other) -> bool:
        return type(other) == Ontology and self.onto == other.onto