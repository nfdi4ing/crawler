from abc import ABC, abstractmethod
from src.OntologyObjects.owlready2Wrappers.OntologyClass import AbstractOntologyClass, OntologyClass
from owlready2 import PropertyClass as OWLProperty

class AbstractOntologyProperty(ABC):

    @abstractmethod
    def get_domain(self) -> list[AbstractOntologyClass]:
        raise NotImplementedError

    @property
    def prefLabel(self) -> str:
        raise NotImplementedError

    @property
    def label(self) -> str:
        raise NotImplementedError

    @property
    def name(self) -> str:
        raise NotImplementedError

class OntologyProperty(AbstractOntologyProperty):

    def __init__(self, property: OWLProperty, lang: str = 'en'):
        self.prop = property
        self.lang = lang

    def get_domain(self):
        return [OntologyClass(x, self.lang) for x in self.prop.get_domain()]

    def get_range(self):
        return [OntologyClass(x, self.lang) for x in self.prop.get_range()]

    @property
    def prefLabel(self) -> str:
        try:
            if self.prop.prefLabel is not None and len(self.prop.prefLabel) > 0:
                prefLabels = getattr(self.prop.prefLabel, self.lang)
                return prefLabels[0] if len(prefLabels) > 0 else ''
            return ''
        except AttributeError:
            return ""

    @property
    def label(self) -> str:
        try:
            if self.prop.label is not None and len(self.prop.label) > 0:
                labels = getattr(self.prop.label, self.lang)
                return labels[0] if len(labels) > 0 else ''
            return ""
        except AttributeError:
            return ""

    @property
    def name(self) -> str:
        try:
            if self.prop.name is not None:
                return self.prop.name
            return ""
        except AttributeError:
            return ""