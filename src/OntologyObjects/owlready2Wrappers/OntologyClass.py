from abc import ABC, abstractmethod
from owlready2 import ThingClass
from owlready2 import EntityClass as OWLOntologyClass

class AbstractOntologyClass(ABC):

    @property
    def prefLabel(self) -> str:
        raise NotImplementedError

    @property
    def label(self) -> str:
        raise NotImplementedError

    @property
    def name(self) -> str:
        raise NotImplementedError

    @abstractmethod
    def is_a(self) -> list['AbstractOntologyClass']:
        raise NotImplementedError

    @abstractmethod
    def subclasses(self) -> list['AbstractOntologyClass']:
        raise NotImplementedError

    @abstractmethod
    def restrictions(self) -> list[str]:
        raise NotImplementedError

    @abstractmethod
    def __eq__(self, other) -> bool:
        raise NotImplementedError

    @abstractmethod
    def __hash__(self) -> int:
        raise NotImplementedError

class OntologyClass(AbstractOntologyClass):

    def __init__(self, OntologyClass: OWLOntologyClass, lang: str = 'en'):
        self.OntologyClass = OntologyClass
        self.lang = lang

    @property
    def prefLabel(self) -> str:
        try:
            if self.OntologyClass.prefLabel is not None and len(self.OntologyClass.prefLabel) > 0:
                prefLabels = getattr(self.OntologyClass.prefLabel, self.lang)
                return prefLabels[0] if len(prefLabels) > 0 else ''
            return ""
        except AttributeError:
            return ""

    @property
    def label(self) -> str:
        try:
            if self.OntologyClass.label is not None and len(self.OntologyClass.label) > 0:
                labels = getattr(self.OntologyClass.label, self.lang)
                return labels[0] if len(labels) > 0 else ''
            return ""
        except AttributeError:
            return ""

    @property
    def name(self) -> str:
        try:
            if self.OntologyClass.name is not None:
                return self.OntologyClass.name
            return ""
        except AttributeError:
            return ""

    def is_a(self):
        return [OntologyClass(x, self.lang) for x in self.OntologyClass.is_a if isinstance(x, ThingClass)]

    def subclasses(self):
        return [OntologyClass(x, self.lang) for x in self.OntologyClass.subclasses()]

    def restrictions(self):
        return [repr(x) for x in self.OntologyClass.is_a if not isinstance(x, ThingClass)]

    def __eq__(self, other):
        return type(other) == OntologyClass and self.OntologyClass == other.OntologyClass

    def __hash__(self):
        return hash(self.OntologyClass)