from .utils import get_label, find_class_for_prop
from .owlready2Wrappers.OntologyClass import AbstractOntologyClass
from .owlready2Wrappers.OntologyProperty import AbstractOntologyProperty


class OntologyBuilder:
    """! This class provides a method for constructing a dictionary that best represents the ontology for file IO.
    For that, it needs the classes mapped to their corresponding properties as input.
    """

    def build(self, class_property_dict:dict, preprocessing_cache:dict | None = None) -> dict:
        """! This method builds a dictionary wherein class labels map to property dictionaries.
        These property dictionaries contain the quantities for the different properties.
        The method uses static default quantities without a config.

        If a config is supplied:
        All property quantities are overwritten as defined in the config.
        This specifically entails the following behavior:
            - New properties can be introduced via quantifying them in the config.
            - These cardinalities can be inherited and overwritten by child classes.
            - If no cardinalities are specified in neither child nor parent, the default_prop_count will be used.
            - If no default_prop_count is supplied, [0] * count will be used and the property effectively ignored.

        @param class_property_dict The dictionary mapping class labels to their associated properties.

        @return A dictionary with the class labels as keys and their corresponding property labels as keys of secondary dictionaries containing their cardinality as lists.
        """
        # goal: get the correct string represantation of classes/properties
        instance_property_dict = {}
        if preprocessing_cache == None:
            preprocessing_cache = {}

        # properties is a list
        owl_class: AbstractOntologyClass
        properties: list[AbstractOntologyProperty]
        for owl_class, properties in class_property_dict.items():
            # extract the correct label
            class_label: str = get_label(owl_class)
            # get parent labels for  "__is_subclass_of__"
            parent_label_list: list[str] = [get_label(parent) for parent in owl_class.is_a()]
            # get all property labels and assign them to the class label
            instance_property_dict[class_label] = {
                "__count__": 1,
                "__is_subclass_of__": parent_label_list,
                "__restrictions__": owl_class.restrictions(),
            }
            if not preprocessing_cache:
                prop: AbstractOntologyProperty
                for prop in properties:
                    instance_property_dict[class_label][get_label(prop)] = [1]
            else:
                count: int = preprocessing_cache[class_label]["count"]
                if count == -1:
                    # if there was a config supplied and no count for this class specified in the config 
                    raise ValueError(
                        f"Class {class_label} was supposed to be included in the dictionary, but neither was a count attribute specified, nor a default_count of > 0!"
                    )
                instance_property_dict[class_label]["__count__"] = count
                prop: str
                for prop in preprocessing_cache[class_label]["counts"]:
                    count: list[int] = preprocessing_cache[class_label]["counts"][prop]
                    if count == [-1]:
                        # if there was a config supplied and no count for this property specified in the config 
                        raise ValueError(f"Property \"{prop}\" of class \"{class_label}\" was supposed to be included in the dictionary, but neither was a count attribute specified, nor a default_prop_count of sum > 0")
                    instance_property_dict[class_label][prop] = count

        return instance_property_dict

    def preprocessing(self, class_property_dict:dict, config:dict | None = None) -> dict:
        """! This method does preprocessing on a given class-property-dict and config
        @param class_property_dict A dictionary mapping owl classes to a list of their properties
        @param config A config loaded from a given toml file

        @return A dict that mimics the configs look where:
        - counts are put for elements where the count is > 0
        - elements do not appear where the count is = 0
        - elements for which nothing is specified appear with a count of -1
        """
        if config == None:
            config = {}
            
        cache = {}
        owl_class: AbstractOntologyClass
        properties: list[AbstractOntologyProperty]
        for owl_class, properties in class_property_dict.items():
            # extract the correct label
            class_label:str = get_label(owl_class)
            cache[class_label] = {}
            if not config:
                cache[class_label]["count"] = -1
            else:
                try:
                    potential_class: AbstractOntologyClass = find_class_for_prop(owl_class, "count", config)
                    potential_class = potential_class if potential_class else owl_class
                    cache[class_label]["count"] = config[get_label(potential_class)][
                        "count"
                    ]
                except KeyError:
                    default_count: int = config.get("default_count", -1)
                    if default_count != 0:
                        # add the given class if 
                        # a) its count is > 0 and thus it is to be included
                        # or b) nothing is specified (= -1) and we raise an error later
                        # after the user had the chance to fill in the value interactively
                        cache[class_label]["count"] = default_count
                    else:
                        cache[class_label]["count"] = -1

            cache[class_label]["counts"] = {}
            prop: AbstractOntologyProperty
            for prop in properties:
                prop_label: str = get_label(prop)
                if not config:
                    cache[class_label]["counts"][prop_label] = [-1]
                else:
                    potential_class: AbstractOntologyClass = find_class_for_prop(
                        owl_class, prop_label, config, ["counts"]
                    )
                    class_with_property = potential_class if potential_class else owl_class
                    # first get the default property count specified for a given class
                    # if none is specified, get the property count specified for all classes
                    # if that is also not specified, just set it to [-1] and raise 
                    # an error later so that the user can still fill the value in interactively
                    default_prop_count = [-1]
                    default_prop_count_class: AbstractOntologyClass = find_class_for_prop(
                        class_with_property, "default_prop_count", config
                    )
                    default_prop_count_class = (
                        default_prop_count_class
                        if default_prop_count_class
                        else class_with_property
                    )
                    try:
                        default_prop_count = config[get_label(default_prop_count_class)][
                            "default_prop_count"
                        ]

                    except KeyError:
                        default_prop_count = config.get("default_prop_count", [-1])

                    # update the quantity
                    prop_count = -1
                    try:
                        prop_count = config[get_label(class_with_property)]["counts"][
                            prop_label
                        ]
                    except KeyError:
                        prop_count = default_prop_count

                    if sum(prop_count) == 0:
                        print(
                            f'The cardinality of property "{prop_label}" of class "{class_label}" was specified to be 0. It will thus be left out.'
                        )
                    else:
                        cache[class_label]["counts"][prop_label] = prop_count

            if config:
                # cycle through properties that are newly defined in the config
                search_list: list[AbstractOntologyClass] = [owl_class]
                # and also check for properties newly defined in parents
                search_list.extend(owl_class.is_a())
                # bfs through parents for properties unbeknownst to this class
                property_labels: list[str] = list(map(get_label, properties))
                # this searches through ALL parents of all classes that are filled by this method!
                # TODO this might be improved by building up a cache of some sort if the inheritance graph is very wide
                # could be of the sort cache: parent -> (great)*grandparent where that (great)*grandparent has a newly defined property
                parent: AbstractOntologyClass
                for parent in search_list:
                    parent_label: str = get_label(parent)
                    # is parent in config?
                    if parent_label in config:
                        for prop_name in [
                            p
                            for p in config[parent_label].get("counts", [])
                            if p not in property_labels
                            and sum(config[parent_label]["counts"][p]) > 0
                        ]:
                            # for all property labels of that parent that are not part of our property label list
                            cache[class_label]["counts"][prop_name] = config[parent_label][
                                "counts"
                            ][prop_name]

                    search_list.extend(parent.is_a())

        return cache
