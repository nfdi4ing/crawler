from src.OntologyObjects.utils import get_label
from src.OntologyObjects.owlready2Wrappers.Ontology import AbstractOntology
from src.OntologyObjects.owlready2Wrappers.OntologyClass import AbstractOntologyClass
from src.OntologyObjects.owlready2Wrappers.OntologyProperty import AbstractOntologyProperty
from src.OntologyObjects.owlready2Wrappers.OntologyReader import AbstractOntologyReader

class OntologyExtractor:
    """! This class extracts the classes and their properties from a given ontology as a dictionary."""

    def __init__(self, ontology_reader_: AbstractOntologyReader, config: dict | None = None, crawl_all_classes: bool = False):
        """! Initializes the OntologyExtractor.

        @param ontology_reader_ The AbstractOntologyReader instance to use for this extractor
        @param config A dictionary yielded by the toml file provided as argument. Default: {}
        @param crawl_all_classes A boolean whether or not all classes are to appear in the output. By default, only the leaf classes appear.
        """
        self.ontology_reader_ = ontology_reader_
        if config == None:
            config = {} 
        self.config = config

        # either "include classes" or "exclude_classes" otherwise raise error
        self.class_filter = ""
        # True if class_filter = "include_classes" otherwise False
        self.inclusion_test: bool = None
        self.crawl_all_classes = crawl_all_classes

        if self.config:
            if "include_classes" in self.config:
                self.class_filter = "include_classes"
                self.inclusion_test = True
            elif "exclude_classes" in self.config:
                self.class_filter = "exclude_classes"
                self.inclusion_test = False
            else:
                raise ValueError(
                    'No "include_classes" nor "exclude_classes" defined in the given config!'
                )

    def extract(self, path: str) -> dict:
        """! Reads the ontology and extracts a dictionary that has the classes as keys and the corresponding
        object / data properties as values.

        @param path Path to ontology
        @return The filled out class -> properties dict
        """
        # read in ontology and load it
        onto: AbstractOntology = self.ontology_reader_.get_ontology(path)
        loaded_onto: AbstractOntology = onto.load()
        return self.get_classes_with_properties(loaded_onto)

    def get_classes_with_properties(self, ontology: AbstractOntology) -> dict:
        """! This method takes a given ontology and creates a dictionary filled with its classes their properties.

        @param ontology Instance of AbstractOntology to process
        @return A dictionary with all classes as keys and their respective properties as values
        """
        # get all props
        properties: list[AbstractOntologyProperty] = self.get_onto_properties(ontology)
        # get all classes as list and assign them to each other (classes and props)
        return self.fill_prop_dict(list(ontology.classes()), properties)

    def get_onto_properties(self, ontology: AbstractOntology) -> list[AbstractOntologyProperty]:
        """! This method filters the properties occurring in the ontology for those whose domain size is greater 0.

        @param ontology Instance of AbstractOntology to get all properties of
        @return A list of all properties for which |domain| > 0
        """
        properties = []
        onto_props: list[AbstractOntologyProperty] = ontology.properties()
        # ignore properties without a domain
        for x in onto_props:
            if len(x.get_domain()) > 0:
                properties.append(x)
        return properties

    def fill_prop_dict(self, classes: list[AbstractOntologyClass], properties: list[AbstractOntologyProperty]):
        """! This method performs the assignment of properties to their respective classes.
        If a config is supplied, classes are included/excluding according to it.

        @param properties Instance of AbstractOntology to process
        @return A dictionary with all classes as keys and their respective properties as values
        """
        class_property_dict = {}
        owl_class: AbstractOntologyClass
        for owl_class in classes:

            # Check if property domain classes correspond to current class (or its ancestors)
            if not self.crawl_all_classes and len(owl_class.subclasses()) > 0:
                continue

            # if there is a config supplied, apply filter to classes
            # skip classes that are either not in included_classes or
            # that are in excluded_classes.
            if self.config and (
                (
                    get_label(owl_class)
                    not in self.config[self.class_filter]
                )
                == self.inclusion_test
            ):
                continue
            
            owl_property: AbstractOntologyProperty
            for owl_property in properties:
                self.add_to_prop_dict(class_property_dict, owl_class, owl_property)
        return class_property_dict

    def add_to_prop_dict(self, class_property_dict:dict, onto_class: AbstractOntologyClass, onto_property: AbstractOntologyProperty):
        """! This method adds a given property as value to the given class and its parents as keys.
        This is only done if these classes are in the domain of the given property.

        @param class_property_dict The dictionary into which the given property is inserted for the given class and its parents, if applicable
        @param onto_class The class for which to infer parents and add the given property, if applicable
        @param onto_property The property to add to the given class and its parents, if applicable
        """
        domain: list[AbstractOntologyProperty] = onto_property.get_domain()
        parents: list[AbstractOntologyClass] = onto_class.is_a()
        current_prop_list = class_property_dict.get(onto_class, [])
        search_list = [onto_class]
        for owl_class in search_list:
            if owl_class in domain:
                current_prop_list.append(onto_property)
                class_property_dict[onto_class] = current_prop_list
                break
            search_list.extend(owl_class.is_a())