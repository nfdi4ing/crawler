from src.UtilityClasses.Parser import *
from src.UtilityClasses.FileIOWrappers import *
from src.UtilityClasses.Postprocessor import *
from typing import Dict
import concurrent.futures
from concurrent.futures import wait
from src.OntologyObjects.utils import get_label
from src.OntologyObjects.owlready2Wrappers.OntologyClass import OntologyClass


class OntologyHandler:
    """! This class does all the crawling that the crawler needs."""

    parsers = {cls.__name__: cls() for cls in AbstractParser.__subclasses__()}

    # for legacy reasons, also add the old names for the parsers back to the parsers dictionary
    parsers[""] = DefaultParser()
    parsers["default"] = DefaultParser()
    parsers["hdf"] = HdfParser()
    parsers["regex"] = RegexParser()
    parsers["string"] = BasicStringParser()
    parsers["os"] = OSParser()

    post_processors = {cls.__name__: cls() for cls in AbstractPostprocessor.__subclasses__()}

    def __init__(
        self,
        data: dict,
        start_directory="",
        crawl_workers=4,
        config={},
    ):
        """!
        Initializes the OntologyHandler class.
        The parser and postprocessor related parameters are purely used as maps from string to method call.

        @param data The dictionary after the multiplexing stage with filled in paths, patterns and parsers for each entity.
        @param start_directory Path to directory from which to start crawling from.
        @param crawl_workers Number of threads to use for parallel crawling.
        @param config A config dictionary derived from a given toml config file.
        """
        self.data = data
        self.start_directory = start_directory
        self.config = config
        self.crawl_workers = crawl_workers

        self.synonyms = {}
        if self.config and "synonyms" in self.config:
            self.synonyms = self.create_synonyms_dict(self.config["synonyms"])

    def create_synonyms_dict(self, synonyms_list):
        result_dict = {}
        for lst in synonyms_list:
            for element in lst["group"]:
                result_dict[element.lower()] = lst["group"]
        return result_dict

    def get_path_type_pattern(self, entity_str, property_str):
        """! This method returns the quadruple of postprocessor, path, pattern_type, and pattern.
        This quadruple is taken from the data dictionary that was supplied in the constructor of the class.

        @param entity_str The string representation of the entity for which to retreive information.
        @param property_str The string representation of the property of the associated entity for which to retreive information.
        @return The quadruple mentioned above for the specific entity-property combination.
        """
        path = self.data[entity_str][property_str]["path"]
        postprocessor = self.data[entity_str][property_str]["postprocessor"]
        pattern = self.data[entity_str][property_str]["pattern"]
        pattern_type = self.data[entity_str][property_str]["type"]
        return postprocessor, path, pattern_type, pattern

    def read_and_match(self, entity_str, property_str, dict_path, pattern_types, patterns, postprocessors):
        """! This method parses a file and tries to match the content to the given pattern.

        @param paths List of paths that should be provided in an input json file.
        @param pattern_type String specifying the parser to use.
        @param pattern The pattern to match against.
        @param postprocessors A list containing dictionaries containing the two entries "type" and "args" specifying the type of postprocessor and additional arguments if applicable.
        @return
            - If there is no match, an error message is printed and the function returns an empty string.
            - If there is a match, the function returns the postprocessor applied to the match and the given arguments.
        """
        # TODO implement post processors in a meaningful way 
        # postprocessor_type, postprocessor_args = (
        #     postprocessors["type"],
        #     postprocessors["args"],
        # )
        # # set postprocessor to either the supplied one or to the identity ignoring the second argument
        # postprocessor = (
        #     self.postprocessors[postprocessor_type]
        #     if postprocessor_type != ""
        #     else lambda x, y: x
        # )
        dict_path_is_dict = True
        # this is needed for backwards compatability
        if isinstance(dict_path, type("")):
            dict_path_is_dict = False

        # Are the paths in the config file and the dictionary to be searched first. hits here are returned directly
        direct_search_paths = []
        for path in (dict_path["paths"] if dict_path_is_dict else [dict_path]):
            direct_search_paths.append({"origin": "Path specified in dictionary under \'paths\'", "path": path})
        
        # Are the paths in the config file to be searched after the direct search paths. If no direct hit is found all paths are searched and multiple hits are put to the
        general_search_paths = []
        for path in (dict_path["path"] if dict_path_is_dict else []):
            general_search_paths.append({"origin": "Path specified in dictionary under \'path\'", "path": path})

        # add the paths from the config to be the first path to be searched
        # first the property path, then the entity path
        if self.config:
            for path in self.config.get("paths", []):
                general_search_paths.append({"origin": "Config general path", "path": path}) 

        # add the start directory to the search paths
        if self.start_directory:
            general_search_paths.append({"origin": "start-dir command line parameter", "path": self.start_directory})

        if isinstance(pattern_types, list):
            if len(pattern_types) == 0:
                pattern_types = [""]
        else:
            pattern_types = [pattern_types]

        if isinstance(patterns, list):
            if len(patterns) == 0:
                patterns = [""] 
        else:
            patterns = [patterns]

        match = []
        for pattern_type in pattern_types: 
            for pattern in patterns:
                if pattern_type == "":
                    parser = self.parsers[DefaultParser.__name__]
                    if pattern == "":
                        pattern = property_str
                else:
                    parser = self.parsers[pattern_type]
                match = parser.search_and_match(direct_search_paths, general_search_paths, pattern, self.synonyms)


                # the very first (pattern_type, pattern) tuple that matches something will return 
                if len(match) == 1:
                    return match[0]["match"]   
                elif len(match) > 1:
                    # TODO implement postprocessors in a meaningful way
                    return match

        if len(match) == 0:
            print(f"No match has been found for property: {property_str} in entity: {entity_str}")
            return ""

    def handle(self):
        """! This method steps through all entities in the data member dictionary and crawls the information for each entity.

        @return A dictionary mapping entities to their complete crawled information.
        """
        filled_ontology = {}
        save_state = {}
        # iterate over all class labels
        for entity in self.data:
            # crawl properties
            entity_info, multiple_matches_savestate = self.crawl_entity_info(entity)

            filled_ontology[entity] = entity_info
            save_state[entity] = multiple_matches_savestate

        return filled_ontology, save_state

    def crawl_property(self, entity_str, property_str, entity_information, multiple_matches_savestate):
             
        """! This method crawls for a given entity its property.
        The file/text to crawl through is specified in the data member dictionary.

        @param entity_str The string representation of a specific entity.
        @param property_str The string representation of a specific property.
        @param entity_information A dictionary into which the crawled property is inserted into.
        """
        # take out the corresponding postprocessor, path, pattern type and pattern
        postprocessor, dict_path, pattern_type, pattern = self.get_path_type_pattern(
            entity_str, property_str
        )

        try:
            # find the string in the file provided
            match = self.read_and_match(entity_str, property_str, dict_path, pattern_type, pattern, postprocessor)

            if isinstance(match, list):
                multiple_matches_savestate[property_str] = match
                entity_information[property_str] = "Multiple matches; use conflict resolution!"
            else:
                entity_information[property_str] = match
        except FileNotFoundError as fnfe:
            print(
                "For entity %s and property %s, no file was found for path %s: "
                % (entity_str, property_str, path),
                fnfe,
            )
            entity_information[property_str] = ""
        except KeyError as ke:
            # TODO rewrite error message here
            print(
                "One of the arguments (post-processor, path, pattern, etc.) provided is not supported or missing: ",
                ke,
            )
            entity_information[property_str] = ""

    def crawl_entity_info(self, entity_str):
        """! This method crawls all information for a given entity according to the definitions on the data member dictionary.

        @param entity_str The string representation of the entity for which to crawl for.
        @return A dictionary containing all crawled information for the given entity.
        """    
        multiple_matches_savestate = {}
        entity_information = {}
        prop_list = self.data[entity_str]
        # iterate over each property and reassign the crawled value
        # this is done in parallel
        with concurrent.futures.ThreadPoolExecutor(self.crawl_workers) as executor:
            futures = {}
            for prop in prop_list:
                if prop != "__restrictions__":
                    future = executor.submit(self.crawl_property, entity_str, prop, entity_information, multiple_matches_savestate)
                    futures[future] = prop
            
            wait(futures)

        return entity_information, multiple_matches_savestate
