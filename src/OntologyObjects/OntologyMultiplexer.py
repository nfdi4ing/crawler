from src.OntologyObjects.utils import (
    determine_crawler_attributes,
    get_label,
    dimensionality_of_list,
    get_property_level_keys,
    get_class_level_keys,
)

from src.OntologyObjects.owlready2Wrappers.OntologyClass import AbstractOntologyClass


class OntologyMultiplexer:

    @staticmethod
    def multiply(data_dict:dict, preprocessing_cache:dict | None = None, config:dict | None = None) -> dict:
        """! This method takes the count for each class, creates as many instances of that class and then 
        creates as many instances of properties as their counts demand for each class instance.
        The crawler attributes are also already added to the class/property instances.

        @param data_dict The dictionary yielded by step 1.
        @param config The config dictionary as yielded by the config toml file.
        @param preprocessing_cache The dictionary yielded by this class's preprocessing() method.

        @return The multiplexed data with all the crawler attributes annotated such as paths and patterns.
        """
        multiplexed_data = {}
        if preprocessing_cache == None:
            preprocessing_cache = {}
        if config == None:
            config = {}

        len_keys_crawler_properties = len(get_class_level_keys())
        # iterate over all class labels
        entity: str
        for entity in data_dict:
            # if __count__ is not a key inside json object towards which the class label is pointing
            # just ignore it
            entity_data: dict = data_dict[entity]
            if "__count__" in entity_data:
                count = entity_data["__count__"]
                if count == 0:
                    # skip classes that have a count of 0
                    continue

                # determine whether there is a config and the entity appears in it
                entity_in_cache: bool = entity in preprocessing_cache
                # check what keys appear in the config
                prop_level_conditions: list[bool] = [
                    (entity_in_cache and (x in preprocessing_cache[entity]))
                    for x in get_property_level_keys()
                ]
                class_level_conditions: list[bool] = [
                    (entity_in_cache and (x in preprocessing_cache[entity]))
                    for x in get_class_level_keys()
                ]

                # copy over the properties
                prop: str
                value: list[int] | list[str]
                for prop, value in entity_data.items():
                    if prop in ["__count__", "__is_subclass_of__"]:
                        continue

                    # get the number of times the property must be multiplied with v[counter] (counter is the
                    # current index of the property) and add it that many times
                    # add index to property name if for that entity the count of the property is > 1
                    # loop "count" number of times
                    for counter in range(count):
                        # the entity's name extended by a counting variable if there is more than 1
                        entity_mult_name = entity + f"{'_' + str(counter + 1) if count > 1 else ''}"
                        # get current data / create new dict if none is present
                        multiplexed_entity_data = multiplexed_data.get(
                            entity_mult_name, {}
                        )

                        if prop == "__restrictions__":
                            multiplexed_entity_data[prop] = value
                            multiplexed_data[entity_mult_name] = multiplexed_entity_data
                            continue

                        iter_range: range | None = None
                        try:
                            iter_range = range(value[counter])
                        except IndexError as ie:
                            raise Exception(
                                f'The length of the list of property counts of property "{prop}" is less than the count of class "{entity}"!'
                            ) from ie

                        for i in iter_range:
                            # the property's name extended by a counting variable if there is more than 1
                            prop_mult_name = prop + f"{'_' + str(i+1) if value[counter] > 1 else ''}"

                            # initialize maps of (property|class) level keys to empty lists
                            property_level_crawler_attr = {}
                            class_level_crawler_attr = {}
                            for prop_lv_key, class_lv_key in zip(
                                get_property_level_keys(), get_class_level_keys()
                            ):
                                property_level_crawler_attr[prop_lv_key] = []
                                class_level_crawler_attr[class_lv_key] = []

                            if entity_in_cache:
                                # loop over all (property|class) level keys and zip them with a counting variable and the different
                                # boolean conditions for whether a key is present in the config (for a property)
                                for j, (
                                    prop_condition,
                                    class_condition,
                                    property_level_key,
                                    class_level_key,
                                ) in enumerate(
                                    zip(
                                        prop_level_conditions,
                                        class_level_conditions,
                                        get_property_level_keys(),
                                        get_class_level_keys(),
                                    )
                                ):
                                    # if there is a config, property_level_key might be provided there which could be filled in now
                                    if (
                                        prop_condition
                                        and preprocessing_cache[entity][
                                            property_level_key
                                        ][prop]
                                    ):
                                        # there is always only one parent as the prop is then removed from the search directory
                                        parent = preprocessing_cache[entity][
                                            property_level_key
                                        ][prop][0]
                                        parent_paths_dimensionality = (
                                            dimensionality_of_list(
                                                config[parent][property_level_key][prop]
                                            )
                                        )
                                        property_level_crawler_attr[
                                            property_level_key
                                        ] = determine_crawler_attributes(
                                            parent,
                                            [property_level_key, prop],
                                            config,
                                            parent_paths_dimensionality,
                                            counter,
                                            i,
                                        )
                                    else:
                                        # is there maybe also an additional list given by one or more parents
                                        class_level_parents = preprocessing_cache[
                                            entity
                                        ][class_level_key]
                                        for parent in class_level_parents:
                                            parent_dimensionality = (
                                                dimensionality_of_list(
                                                    config[parent][class_level_key]
                                                )
                                            )
                                            parent_list = determine_crawler_attributes(
                                                parent,
                                                [class_level_key],
                                                config,
                                                parent_dimensionality,
                                                counter,
                                                i,
                                            )
                                            class_level_crawler_attr[
                                                class_level_key
                                            ].extend(parent_list)

                            # fill in multiplexed data for this entity
                            multiplexed_entity_data[prop_mult_name] = {}
                            for prop_lv_key, class_lv_key in zip(get_property_level_keys(), get_class_level_keys()):
                                if class_lv_key == "path":
                                    # special treatment of file paths as their source 
                                    # needs to be distinguished for the ontology handler
                                    multiplexed_entity_data[prop_mult_name]["path"] = {
                                        "paths": property_level_crawler_attr["paths"],
                                        "path": class_level_crawler_attr["path"],
                                    }
                                else:
                                    multiplexed_entity_data[prop_mult_name][class_lv_key] = (
                                        property_level_crawler_attr[prop_lv_key]
                                        + class_level_crawler_attr[class_lv_key]
                                    )

                        multiplexed_data[entity_mult_name] = multiplexed_entity_data
        return multiplexed_data

    @staticmethod
    def preprocessing(data_dict:dict, config:dict, label_class_dict:dict | None = None) -> dict:
        """! Automates the preprocessing by dynamically executing the needed preprocessing submethods.

        @param data_dict The dictionary yielded by step 1.
        @param config The config dictionary as yielded by the config toml file.
        @param label_class_dict A dictionary mapping each label of a class from data_dict to an actual ontology class.

        @return A dictionary that maps 
            entity -> {"paths": {properties -> []}, "path": [], "pattern": [], "patterns": {properties -> []}, "type": [], "types": {properties -> []}, "postprocessor": [], "postprocessors": {properties -> []}} 
        so that all paths can easily be retreived form the config.
        """
        preprocessing_cache = {}
        if config:
            if label_class_dict:
                # use ontology for full inheritance
                preprocessing_cache = OntologyMultiplexer.preprocessing_bfs(
                    data_dict, config, label_class_dict
                )
            else:
                # use one level inheritance information inherent in data
                entity_props_to_search, preprocessing_cache = (
                    OntologyMultiplexer.preprocessing_preparing(data_dict, config)
                )
                entity: str
                for entity in entity_props_to_search:
                    parent: str
                    for parent in [entity] + data_dict[entity]["__is_subclass_of__"]:
                        if not OntologyMultiplexer.preprocessing_parent_extraction(
                            entity,
                            parent,
                            config,
                            entity_props_to_search,
                            preprocessing_cache,
                        ):
                            break
        else:
            # if no config present, return empty preprocessing cache
            _, preprocessing_cache = (
                OntologyMultiplexer.preprocessing_preparing(data_dict, config)
            )

        return preprocessing_cache

    @staticmethod
    def preprocessing_preparing(data_dict:dict, config:dict | None) -> tuple[dict, dict]:
        """! This method prepares two dictionaries for the preprocessing.

        @param data_dict The dictionary yielded by step 1.
        @param config The config dictionary as yielded by the config toml file.

        @return (entity_props_to_search, bfs_cache) where the two variables are as described in multiplexer_preprocessing_parent_path_extraction().
        """
        if config == None:
            config = {}
            
        entity_props_to_search = {}
        # This dictionary will be the output and will be successively constructed
        bfs_cache = {}
        # Loop through all entities
        entity: str
        for entity in data_dict:
            # prepare dictionaries for this entity
            entity_props_to_search[entity] = {
                "paths": [],
                "types": [],
                "patterns": [],
                "postprocessors": [],
            }

            gen = list(
                p
                for p in data_dict[entity]
                if p not in ["__count__", "__restrictions__", "__is_subclass_of__"]
            )

            bfs_cache[entity] = {
                "paths": {p: [] for p in gen},
                "path": [],
                "type": [],
                "types": {p: [] for p in gen},
                "pattern": [],
                "patterns": {p: [] for p in gen},
                "postprocessors": {p: [] for p in gen},
                "postprocessor": [],
            }
            # Check whether the given entity has a "paths" field in its config
            entity_in_config: bool = entity in config
            paths_in_config: bool = entity_in_config and "paths" in config[entity]
            types_in_config:bool = entity_in_config and "types" in config[entity]
            patterns_in_config:bool = entity_in_config and "patterns" in config[entity]
            postprocessors_in_config: bool = (
                entity_in_config and "postprocessors" in config[entity]
            )
            # Loop over all properties of a given entity
            prop: str
            for prop in data_dict[entity]:
                # Filter them
                if prop in ["__count__", "__restrictions__", "__is_subclass_of__"]:
                    continue

                condition: bool
                key: str
                for condition, key in zip(
                    [
                        paths_in_config,
                        patterns_in_config,
                        types_in_config,
                        postprocessors_in_config,
                    ],
                    get_property_level_keys(),
                ):
                    # Skip those properties, that have directly defined crawler attributes in the config
                    if condition and config[entity][key].get(prop):
                        bfs_cache[entity][key][prop] = [entity]
                    else:
                        # Append properties that have to be searched to dictionaries
                        entity_props_to_search[entity][key].append(prop)

        return entity_props_to_search, bfs_cache

    @staticmethod
    def preprocessing_bfs(data_dict:dict, config:dict, label_class_dict:dict) -> dict:
        """! This method performs preprocessing to utilize inheritance information provided by an ontology.

        @param data_dict The dictionary of flat classes from step 1.
        @param config The config dictionary derived from the input toml file.
        @param label_class_dict A dictionary mapping each label of a class from data_dict to an actual ontology class.

        @return A dictionary that maps 
            entity -> {"paths": {properties -> []}, "path": [], "pattern": [], "patterns": {properties -> []}, "type": [], "types": {properties -> []}, "postprocessor": [], "postprocessors": {properties -> []}} 
        so that all paths can easily be retreived form the config.
        """
        # First, define for which entity what properties have to be searched/inherited
        entity_props_to_search, bfs_cache = OntologyMultiplexer.preprocessing_preparing(
            data_dict, config
        )
        # Second, do the actual breadth-first search for all entities
        entity: str
        for entity in entity_props_to_search:
            entity_class: AbstractOntologyClass = label_class_dict[entity]
            search_list: list[AbstractOntologyClass] = [entity_class]
            # BFS
            owl_class: AbstractOntologyClass
            for owl_class in search_list:
                class_label: str = get_label(owl_class)
                # extract all relevant path and paths fields for given parent label
                if not OntologyMultiplexer.preprocessing_parent_extraction(
                    entity, class_label, config, entity_props_to_search, bfs_cache
                ):
                    break
                search_list.extend(owl_class.is_a())

        return bfs_cache

    @staticmethod
    def preprocessing_parent_extraction(
        entity:str, class_label:str, config:dict, entity_props_to_search:dict, bfs_cache:dict
    ) -> bool:
        """! This method extracts all relevant path and paths records from the config dictionary.

        @param entity The entity for which a search is currently running.
        @param class_label A label of any parent of entity.
        @param config The config yielded by the config toml file.
        @param entity_props_to_search A dictionary that maps entity -> [properties] where properties have no direct paths given in the config.
        @param bfs_cache The cache to fill when a path or paths entry is found for a (entity, property) pair.

        @return True if not all properties have a direct path assigned. False otherwise.
        """
        # Check whether there is information on the given class in the config
        class_in_config: bool = class_label in config
        if not class_in_config:
            return True

        class_keys: list[str] = config[class_label]
        
        # check for information present in config 
        any_general_info_in_config = any(
            x in class_keys for x in get_class_level_keys()
        )
        any_specific_info_in_config = any(
            x in class_keys for x in get_property_level_keys()
        )

        # If any crawler attributes are defined
        if any_general_info_in_config or any_specific_info_in_config:
            if any_general_info_in_config:
                for key in get_class_level_keys():
                    if key in class_keys:
                        bfs_cache[entity][key].append(class_label)

            if any_specific_info_in_config:
                property_level_conditions: list[bool] = [
                    (x in class_keys) for x in get_property_level_keys()
                ]

                # loop through all properties of a given class that have been determined beforehand
                key: str
                condition: bool
                for key, condition in zip(
                    get_property_level_keys(), property_level_conditions
                ):
                    for prop in entity_props_to_search[entity][key]:
                        if condition and config[class_label][key].get(prop):
                            # if there is a directly defined path for a property, we can append the label of the class that specifies it to the cache
                            bfs_cache[entity][key][prop].append(class_label)
                            # and remove the property from the properties to be searched
                            entity_props_to_search[entity][key].remove(prop)

        if all(
            not entity_props_to_search[entity][x] for x in get_property_level_keys()
        ):
            # if we now ran out of properties to search, we can finish the bfs for this 'leaf class'
            return False

        return True
