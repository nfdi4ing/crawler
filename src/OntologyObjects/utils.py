from .owlready2Wrappers.OntologyClass import AbstractOntologyClass
from .owlready2Wrappers.OntologyProperty import AbstractOntologyProperty
from functools import reduce
from operator import getitem

@staticmethod
def get_property_level_keys() -> list[str]:
    return ["paths", "patterns", "types", "postprocessors"]

@staticmethod
def get_class_level_keys() -> list[str]:
    return ["path", "pattern", "type", "postprocessor"]

@staticmethod
def find_class_for_prop(leaf_class: AbstractOntologyClass, prop_label: str, config: dict, subkeys: list[str] = []) -> AbstractOntologyClass | None:
    """! This method uses breadth-first search for a definition of a given property in the config.

    @param leaf_class The class from which to examine itself and its parents.
    @param prop_label The label of the property that is currently searched for.
    @param config The config dictionary in which old and new properties can be defined.
    @param subkeys List of keys from which to expend the class' dictionary entry. By default []

    @return The class that has a definition for the given property. If there is none, None will be returned.
    """
    search_list: list[AbstractOntologyClass] = [leaf_class]
    for owl_class in search_list:
        class_label: str = get_label(owl_class)
        config_entry = None

        # find entry for the current class in config
        try:
            config_entry = reduce(getitem, [class_label]+subkeys+[prop_label], config)
        except KeyError:
            pass

        if config_entry:
            #if there is an entry for the prop_label for the current class
            return owl_class
        else:
            # otherwise proceed to search in parents
            parents: list[AbstractOntologyClass] = owl_class.is_a()
            search_list.extend(parents)
    return None

@staticmethod
def determine_crawler_attributes(entity_str: str, subkeys: list[str], config: dict, dimensionality: int, *indices: list[int]) -> list[str]:
    """! This method extracts a crawler attribute (i.e. path, paths + prop, ...)
    It uses the dimensionality attribute to access indices as needed to find the most specific crawler property value
    that is provided in the config.

    @param entity_str The string represenation of an entity.
    @param subkeys A list of keys to follow along in the config from config[entity_str] onwards.
    @param dimensionality The dimensionality of the list as given by dimensionaliy_of_list(ls).
    @param indices Optional list of indices where each index is used for the next deeper level.

    @return A most specific list of the crawler property that was searched for
    """
    attribute_list: list[str] | list[list[str]] | list[list[list[str]]] = reduce(getitem, [entity_str]+subkeys, config)
    # if there is a property instance with its own list
    if dimensionality > 1:
        # extract corresponding list/property for property
        attribute_list = attribute_list[indices[0]]
        currently_a_list = attribute_list and isinstance(attribute_list[0], list) 
        # if there might be a definition for every instance of the property_str
        if dimensionality > 2 and currently_a_list:
            attribute_list = attribute_list[indices[1]]

    return list(attribute_list)

@staticmethod
def dimensionality_of_list(ls: list[object] | list[list[object]] | list[list[list[object]]]) -> int:
    """! This function determines the dimensionality of a given list as defined in the following way:
    1 <= any list is of dimensionality 1 
    2 <= any list of dimensionality 1 that itself consists of multiple lists
    3 <= any list of dimensionality 2 where each element is a list of lists.

    @param ls The list to determine the dimensionality for.
    @return The dimensionality number of the given list in 1:3
    """
    dimensionality = 1
    if len(ls) > 0:
        dimensionality = dimensionality + int(isinstance(ls[0], list))
        if dimensionality == 2:
            dimensionality = dimensionality + int(any(isinstance(ps[0], list)  for ps in ls if ps))
    return dimensionality

@staticmethod
def get_label(entity: AbstractOntologyClass | AbstractOntologyProperty) -> str:
    """! This method returns a label for a given entity.

    @param entity The OWL entity to return a labelling for.

    @return
            - If the entity has a non-empty preferred label, return this preferred label.
            - Else if the entity has a non-empty label, return this label.
            - Otherwise return the entity's name.
    """
    # this is subject to change but I think it's most reasonable like this
    if entity.prefLabel is not None and entity.prefLabel != "":
        return entity.prefLabel
    if entity.label is not None and entity.label != "":
        return entity.label
    else:
        return entity.name