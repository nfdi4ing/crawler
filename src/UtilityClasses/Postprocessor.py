import numpy as np
import json
import re
from abc import ABC, abstractmethod

class AbstractPostprocessor(ABC):

    @abstractmethod
    def __call__(self, default: str, args: str = None):
        raise NotImplementedError


def parse_str(str_list: str) -> []:
    """Remember to document that there should use only double quotes inside the string!"""
    return json.loads(str_list)


class MinPP(AbstractPostprocessor):

    def __call__(self, default: str, args: str = None):
        return np.min(
            parse_str("%s" % (default if args is None or args == "" else args))
        )


class MedianPP(AbstractPostprocessor):

    def __call__(self, default: str, args: str = None):
        return np.median(
            parse_str("%s" % (default if args is None or args == "" else args))
        )


class MaxPP(AbstractPostprocessor):

    def __call__(self, default: str, args: str = None):
        return np.max(
            parse_str("%s" % (default if args is None or args == "" else args))
        )


class StdDevPP(AbstractPostprocessor):

    def __call__(self, default: str, args: str = None):
        return np.std(
            parse_str("%s" % (default if args is None or args == "" else args))
        )


class MeanPP(AbstractPostprocessor):

    def __call__(self, default: str, args: str = None):
        return np.mean(
            parse_str("%s" % (default if args is None or args == "" else args))
        )


class VarPP(AbstractPostprocessor):

    def __call__(self, default: str, args: str = None):
        return np.var(
            parse_str("%s" % (default if args is None or args == "" else args))
        )


class SqrtPP(AbstractPostprocessor):

    def __call__(self, default: str, args: str = None) -> []:
        return np.sqrt(
            parse_str("%s" % (default if args is None or args == "" else args))
        ).tolist()


class Log2PP(AbstractPostprocessor):

    def __call__(self, default: str, args: str = None) -> []:
        return np.log2(
            parse_str("%s" % (default if args is None or args == "" else args))
        ).tolist()


class Log10PP(AbstractPostprocessor):

    def __call__(self, default: str, args: str = None) -> []:
        return np.log10(
            parse_str("%s" % (default if args is None or args == "" else args))
        ).tolist()


class SortPP(AbstractPostprocessor):

    def __call__(self, default: str, args: str = None):
        args = args.lower()
        if args == "a" or args.startswith("asc"):
            return np.sort(parse_str(default)).tolist()
        else:
            return np.sort(parse_str(default))[::-1].tolist()


class RegexPP(AbstractPostprocessor):

    def __call__(self, default: str, args: str = None):
        try:
            args = re.compile(args)
            match = re.search(args, default)
            return match.groups(1)[0]
        except:
            return ""
