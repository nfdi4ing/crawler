import h5py
import numpy
import os
import re
from abc import ABC, abstractmethod
import glob
from typing import List

class AbstractParser(ABC):

    @abstractmethod
    def origin_is_file(self) -> bool:
        """! This method should return a boolean value indicating whether the origin is a file or not

        @return
            - True if the origin is a file
            - False if the origin is not a file
        """
        raise NotImplementedError
    
    @abstractmethod
    def search(self, pattern: str, text: str, synonyms: dict) -> str:
        """! This method should return the match of the pattern in the text

        @param pattern Is the keyword that the parser should search for in the text
        @param text Is the text in which the parser should search for the pattern
        @param synonyms Is a dictionary containing synonyms for the pattern
        @return
            - The match of the pattern in the text
        """

        raise NotImplementedError
    
    # This method should return the match of the pattern in the origin
    # The origin is the path to the file or the text in which the parser should search for the pattern
    @abstractmethod
    def try_origin(self, origin: str, pattern: str, synonyms: dict) -> str:
        """! This method should return the match of the pattern in the text

        @param origin Is the path to the file or other origin in which the parser should search for the pattern
        @param pattern Is the pattern that the parser should search for in the origin
        @param synonyms Is a dictionary containing synonyms for the pattern
        @return
            - The match of the pattern in the origin
        """

        return self.search(pattern, "")
    
    def search_and_match(self, direct_search_origins: List[str], general_search_origins: List[str], pattern: str, synonyms: dict) -> str:
        """! This method should return the match of the pattern in the origin

        @param direct_search_origins Is a list of paths to the files or other origins in which the parser should search for the pattern, and the search should be done in the direct paths first
        @param general_search_origins Is a list of paths to the files or other origins in which the parser should search for the pattern
        @param pattern Is the pattern that the parser should search for in the origin
        @param synonyms Is a dictionary containing synonyms for the pattern
        @return
            - The match of the pattern in the origin
        """

        all_results = []

        # search in direct search paths first
        for direct_or_general in [direct_search_origins, general_search_origins]:
            for origin_el in direct_or_general:
                paths = origin_el["path"]
                if not isinstance(paths, list):
                    paths = [ paths ]

                for path in paths:
                    # if the origin is a file, search in the file, else call the try_origin method directly
                    if not self.origin_is_file():
                        match = self.try_origin(path, pattern, synonyms)
                        if match and match not in map(lambda x: x["match"], all_results):
                            all_results.append({"match" : match, "file" : path, "origin" : origin_el["origin"]})
                    else:
                        # if the path is a file, search in the file, else search in all files in the directory
                        options = glob.glob(path, recursive=True)
                        for option in options:
                            if os.path.isfile(option):
                                match = self.try_origin(option, pattern, synonyms)
                                if match and match not in map(lambda x: x["match"], all_results):
                                    all_results.append({"match" : match, "file" : option, "origin" : origin_el["origin"]})
                            else:
                                for root, _, files in os.walk(path):
                                    for file in files:
                                        match = self.try_origin(os.path.join(root, file), pattern, synonyms)
                                        if match and match not in map(lambda x: x["match"], all_results):
                                            all_results.append({"match" : match, "file" : file, "origin" : origin_el["origin"]})

            # stop execution after at least one result is found in direct_search_paths
            if len(all_results) > 0:
                return all_results

        return all_results
        
class DefaultParser(AbstractParser):
    def origin_is_file(self) -> bool:
        return True 
    
    def search(self, pattern: str, text: str, synonyms: dict) -> str:       
        try:
            # Try to match the name of the element, which is in the pattern, to try to find it in the text
            # Search by the following principles
            # 1. The keyword can have multiple different forms. Upper, lowercase, spaces can be replaced by - or _ or nothing.
            # 2. The line contains the keyword. If that is the case the match will be the text after the keyword spaced by either spaces, tabs, = or : or multiple spaces
            # 3. The keyword without anything after it. If that is the case the next line will be matched
            
            keyword = pattern
            for char in ['-', '_', ' ']:
                keyword = keyword.replace(char, '[-_\\s]')

            if pattern.lower() in synonyms:
                keyword = f"(?:{keyword}"

                group = synonyms[pattern.lower()]
                for syn in group:
                    syn = syn.strip().replace(char, '[-_\\s]')
                    keyword += f"|{syn}"

                keyword += ")"
    
            
            file_pattern = re.compile(keyword + r"\s*(?:\:|\=|\_)(.*?)(?=\s{3,}|\t|\n)", re.IGNORECASE | re.MULTILINE)

            match = re.search(file_pattern, text)
            if match is not None:
                match = match.groups()[0].strip()
                
            return match
        except:
            return []
        
        
    def try_origin(self, origin: str, pattern: str, synonyms: dict):
        try:
            # ignore json, owl, yaml, yml, toml files
            if origin.endswith(".json") or origin.endswith(".owl") or origin.endswith(".yaml") or origin.endswith(".yml") or origin.endswith(".toml"):
                return None
            
            with open(origin, encoding='utf-8') as f:
                text = f.read()
                match = self.search(pattern, text, synonyms)
                if match:
                    return match
        except:
            pass

        return None

class HdfParser(AbstractParser):
    def origin_is_file(self) -> bool:
            return True 

    @staticmethod
    def SanityCheckPattern(insane: str):
        if insane[0] not in ['A', 'D']:
            raise ValueError(
                "First letter must specify whether Dataset data (D) or Attribute data (A) is to be read out")
        if insane[1] != '/':
            raise ValueError("Path to data must start with slash")

    def search(self, pattern: str, text: str, synonyms: dict) -> str:
        self.SanityCheckPattern(pattern)
        with h5py.File(text, "r") as h5file:
            datapath = pattern[1:] # The leading 'A' or 'D' is removed here
            if pattern[0] == 'A':
                attribute_path, attribute_name = datapath.rsplit('/',1)  # Attribute name
                attribute_path = attribute_path + "/" # trailing / is removed during split and hdf5 does not like empty paths
                match = h5file[attribute_path].attrs[attribute_name] # Extract attribute value
                # Type checks
                if isinstance(match, numpy.byte): # if "bytestring": decode it to regular string (JSON-serializable)
                    match = match.decode('utf-8')
                elif isinstance(match, numpy.ndarray): # if "ndarray": convert to unicode type and then string (JSON-serializable)
                    match = str(match.astype('U13'))
                else: # else: just convert to string (JSON-serializable)
                    match = str(match)
            else:
                # match = numpy.array2string(numpy.asarray(h5file[datapath]))
                # Commas are needed for JSON loads. The above implementation removes the commas.
                # TODO this seems redundant
                match = numpy.asarray(h5file[datapath])
                match = str(match.tolist())
            return match
    
    def try_origin(self, origin: str, pattern: str, synonyms: dict) -> str:
        pass


class RegexParser(AbstractParser):
    def origin_is_file(self) -> bool:
        return True 
    
    def search(self, pattern: str, text: str, synonyms: dict) -> str:
        try:
            pattern = re.compile(pattern)
            match = re.search(pattern, text)
            return match.groups(1)[0]
        except:
            return ""
        
    def try_origin(self, origin: str, pattern: str, synonyms: dict):   
        try:
            with open(origin, encoding='utf-8') as f:
                text = f.read()
                match = self.search(pattern, text, synonyms)
                if match:
                    return match
        except:
            pass

        return None

class BasicStringParser(AbstractParser):    
    def origin_is_file(self) -> bool:
        return False 
    
    def search(self, pattern: str, text: str, synonyms: dict) -> str:
        return pattern

    def try_origin(self, origin: str, pattern: str, synonyms: dict) -> str:
       return self.search(pattern, "", synonyms)


class OSParser(AbstractParser):
    def origin_is_file(self) -> bool:
        return False 
    
    def search(self, pattern: str, text: str, synonyms: dict) -> str:
        stream = os.popen(pattern)
        output = stream.read()
        stream.close()
        return output
    
    def try_origin(self, origin: str, pattern: str, synonyms: dict) -> str:
        return self.search(pattern, "", synonyms)
