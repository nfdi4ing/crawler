import json
import yaml
import toml
from abc import ABC, abstractmethod

class AbstractFileIO(ABC):

    @abstractmethod
    def dump(self, data: dict, out_path: str):
        raise NotImplementedError

    @abstractmethod
    def load(self, path: str) -> dict:
        raise NotImplementedError


class JsonIO(AbstractFileIO):

    def dump(self, data: dict, path: str):
        with open(path, 'w', encoding='utf-8') as f:
            json.dump(data, f, ensure_ascii=False, indent=4)

    def load(self, path: str) -> dict:
        with open(path, encoding='utf-8') as file:
            return json.load(file)

class TomlIO(AbstractFileIO):

    def dump(self, data: dict, path:str):
        with open(path, 'w', encoding='utf-8') as f:
            toml.dump(data, f)

    def load(self, path:str) -> dict:
        return toml.load(path)

class YamlIO(AbstractFileIO):
    class Dumper(yaml.SafeDumper):
        def ignore_aliases(self, data):
            return True

    def dump(self, data: dict, path: str):
        # if the following 7 lines are not present, yaml automatically adds aliases and anchors (as in the .gitlab-ci.yml)
        yaml.SafeDumper.org_represent_str = yaml.SafeDumper.represent_str

        def repr_str(dumper, data):
            return dumper.represent_scalar(u'tag:yaml.org,2002:str', data, style='"')

        yaml.add_representer(str, repr_str, Dumper=yaml.SafeDumper)

        with open(path, 'w', encoding='utf-8') as f:
            yaml.dump(data, f, indent=4, default_flow_style=False, Dumper=self.Dumper, sort_keys=False)

    def load(self, path: str) -> dict:
        with open(path, "r", encoding='utf-8') as stream:
            return yaml.safe_load(stream)

@staticmethod
def get_file_extension(path: str, supported_endings) -> str:
    ending = path.rpartition(".")[-1] # Skip dot
    if ending not in supported_endings: # Checking if the extracted file extension is not supported
        raise ValueError(
            f"File extension {ending} not supported. Please provide a file extension from this list {supported_endings}.")

    return ending