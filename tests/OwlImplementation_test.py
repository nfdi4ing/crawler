import json
import unittest
import unittest.mock

from src.OntologyObjects.owlready2Wrappers.OntologyClass import OntologyClass
from src.OntologyObjects.owlready2Wrappers.OntologyReader import OntologyReader
from src.OntologyObjects.owlready2Wrappers.OntologyProperty import OntologyProperty
from src.OntologyObjects.owlready2Wrappers.Ontology import Ontology
from src.OntologyObjects.OntologyMultiplexer import OntologyMultiplexer
from src.UtilityClasses import Parser, Postprocessor
from src.UtilityClasses.FileIOWrappers import get_file_extension
import numpy as np
import owlready2

dict_for_json_multiplexer = {
    "Pizza": {
        "__count__": 3,
        "price": [1, 1, 1],
        "has_main_topping": [1, 1, 1],
        "has_topping": [1, 1, 1],
    },
    "Topping": {
        "__count__": 1,
        "main_topping_of": [1, 1, 1],
        "topping_of": [1, 1, 1],
    },
    "Will_be_deleted": {
        "__count__": 0,
    }
}
dict_after_json_multiplexer = {
    "Pizza_1": {
        "price": {
            "postprocessor": []
            ,
            "path": 
                {
                    "paths": [],
                    "path": []
                }
            ,
            "type": 
                []
            ,
            "pattern": 
                []
            
        },
        "has_main_topping": {
            "postprocessor": []
            ,
            "path": 
                {
                    "paths": [],
                    "path": []
                }
            ,
            "type": 
                []
            ,
            "pattern": 
                []
            
        },
        "has_topping": {
            "postprocessor": []
            ,
            "path": 
                {
                    "paths": [],
                    "path": []
                }
            ,
            "type": 
                []
            ,
            "pattern": 
                []
            
        }
    },
    "Pizza_2": {
        "price": {
            "postprocessor": []
            ,
            "path": 
                {
                    "paths": [],
                    "path": []
                }
            ,
            "type": 
                []
            ,
            "pattern": 
                []
            
        },
        "has_main_topping": {
            "postprocessor": []
            ,
            "path": 
                {
                    "paths": [],
                    "path": []
                }
            ,
            "type": 
                []
            ,
            "pattern": 
                []
            
        },
        "has_topping": {
            "postprocessor": [],
            "path": 
                {
                    "paths": [],
                    "path": []
                }
            ,
            "type": 
                []
            ,
            "pattern": 
                []
            
        }
    },
    "Pizza_3": {
        "price": {
            "postprocessor": []
            ,
            "path": 
                {
                    "paths": [],
                    "path": []
                }
            ,
            "type": 
                []
            ,
            "pattern": 
                []
            
        },
        "has_main_topping": {
            "postprocessor": []
            ,
            "path": 
                {
                    "paths": [],
                    "path": []
                }
            ,
            "type": 
                []
            ,
            "pattern": 
                []
            
        },
        "has_topping": {
            "postprocessor": []
            ,
            "path": 
                {
                    "paths": [],
                    "path": []
                }
            ,
            "type": 
                []
            ,
            "pattern": 
                []
            
        }
    },
    "Topping": {
        "main_topping_of": {
            "postprocessor": []
            ,
            "path": 
                {
                    "paths": [],
                    "path": []
                }
            ,
            "type": 
                []
            ,
            "pattern": 
                []
            
        },
        "topping_of": {
            "postprocessor": []
            ,
            "path": 
                {
                    "paths": [],
                    "path": []
                }
            ,
            "type": 
                []
            ,
            "pattern": 
                []
            
        }
    }
}


class DummyClass:
    pass



class ParsersTest(unittest.TestCase):
    def test_default(self):
        default = Parser.DefaultParser()
        self.assertEqual(default.search("TEST HEADLINE", """
            Test-Headline: This is a test      Second-Headline: This should not be returned
        """, {}), "This is a test")


        self.assertEqual(default.search("Synonym", """
            Test-Headline: This is a test      Second-Headline: This should not be returned
        """, {"synonym": ["synonym", "Test-Headline"]}), "This is a test")

    def test_hdf(self):
        hdf = Parser.HdfParser()
        pattern_correct = "D/Temperatures/Ambient"
        pattern_wrong_1 = "S/Temperatures/Ambient"
        pattern_wrong_2 = "AA/"

        # Test correct input
        try:
            hdf.SanityCheckPattern(pattern_correct)
        except ValueError:
            self.fail("Hdf Parser raised an error on a correct pattern.")
        # Test wrong input
        with self.assertRaises(ValueError):
            hdf.SanityCheckPattern(pattern_wrong_1)
        with self.assertRaises(ValueError):
            hdf.SanityCheckPattern(pattern_wrong_2)

        actual_return_0 = hdf.search("D/Money/Price", "tests/data/pizza.h5", {})
        actual_return_1 = hdf.search("A/Money/Currency", "tests/data/pizza.h5", {})
        actual_return_2 = hdf.search("A/Topping", "tests/data/pizza.h5", {})

        expected_0, expected_1, expected_2 = "[8.9, 9.25, 11.75, 12.0]", "Euro", "Cheese"

        self.assertEqual(expected_0, actual_return_0, "Expected " + str(expected_0) + " but got " + actual_return_0)
        self.assertEqual(expected_1, actual_return_1, "Expected " + str(expected_1) + " but got " + actual_return_1)
        self.assertEqual(expected_2, actual_return_2, "Expected " + str(expected_2) + " but got " + actual_return_2)

    def test_string(self):
        string = Parser.BasicStringParser()
        test_string = "This will be returned as string."
        match = string.search(test_string, "", {})

        self.assertEqual(match, test_string)

    def test_regex(self):
        regex = Parser.RegexParser()
        text_for_branch_1 = "This is the text for branch 1 ( match here ) ( second group here )"
        text_for_branch_2 = "This is the text for branch 2 "
        pattern = ".* \(( .* )\) "

        search_1 = regex.search(pattern, text_for_branch_1, {})
        search_2 = regex.search(pattern, text_for_branch_2, {})

        self.assertEqual(search_1, " match here ")
        self.assertEqual(search_2, "")

    def test_os(self):
        os_parser = Parser.OSParser()

        match_1 = os_parser.search("echo 'This should be returned as a match!'", "", {})
        match_2 = os_parser.search("echo 'This should be returned as a match!'",
                                   "Some different string", {})  # makes sure parser.search() has no side effects when
        # given additional arguments

        match_str = 'This should be returned as a match!\n'

        self.assertEqual(match_1, match_str)
        self.assertEqual(match_2, match_str)


class JsonMultiplexerTest(unittest.TestCase):

    def test(self):
        new_json_data = OntologyMultiplexer.multiply(dict_for_json_multiplexer)
        self.assertEqual(new_json_data, dict_after_json_multiplexer)


class PostprocessorTest(unittest.TestCase):

    def setUp(self) -> None:
        self.num_list_string = "[1, 2, 3, 3, 4, 5, 6]"

    def test_parse_str(self):
        dict_str = '{"first_entry": 1, "second_entry": 2}'
        num_str = "6"

        parsed_dict = Postprocessor.parse_str(dict_str)
        parsed_list = Postprocessor.parse_str(self.num_list_string)
        parsed_num = Postprocessor.parse_str(num_str)

        self.assertEqual(parsed_dict, {"first_entry": 1, "second_entry": 2})
        self.assertEqual(parsed_list, [1, 2, 3, 3, 4, 5, 6])
        self.assertEqual(parsed_num, 6)

    def test_min(self):
        min_pp = Postprocessor.MinPP()
        output_min = min_pp(self.num_list_string)
        self.assertEqual(output_min, 1)

    def test_median(self):
        median_pp = Postprocessor.MedianPP()
        output_median = median_pp(self.num_list_string)
        self.assertEqual(output_median, 3)

    def test_max(self):
        max_pp = Postprocessor.MaxPP()
        output_max = max_pp(self.num_list_string)
        self.assertEqual(output_max, 6)

    def test_stddev(self):
        stddev_pp = Postprocessor.StdDevPP()
        output_stddev = stddev_pp(self.num_list_string)
        self.assertAlmostEqual(output_stddev, 1.591, 3)

    def test_mean(self):
        mean_pp = Postprocessor.MeanPP()
        output_mean = mean_pp(self.num_list_string)
        self.assertAlmostEqual(output_mean, 3.429, 3)

    def test_var(self):
        var_pp = Postprocessor.VarPP()
        output_var = var_pp(self.num_list_string)
        self.assertAlmostEqual(output_var, 2.5306, 3)

    def test_sqrt(self):
        sqrt_pp = Postprocessor.SqrtPP()
        output_sqrt = sqrt_pp("9")
        self.assertEqual(output_sqrt, 3)

    def test_log2(self):
        log2_pp = Postprocessor.Log2PP()
        output_log2 = log2_pp("8")
        self.assertEqual(output_log2, 3)

    def test_log10(self):
        log10_pp = Postprocessor.Log10PP()
        output_log10 = log10_pp("100")
        self.assertEqual(output_log10, 2)

    def test_sort_asc(self):
        sort_pp = Postprocessor.SortPP()
        output_sort_asc = sort_pp(self.num_list_string, "asc")
        cond = output_sort_asc == np.array([1, 2, 3, 3, 4, 5, 6])
        self.assertTrue(cond.all())

    def test_sort_desc(self):
        sort_pp = Postprocessor.SortPP()
        output_sort_asc = sort_pp(self.num_list_string, "desc")
        cond = output_sort_asc == np.array([6, 5, 4, 3, 3, 2, 1])
        self.assertTrue(cond.all())

    def test_regex(self):
        regex_pp = Postprocessor.RegexPP()
        output_regex1 = regex_pp(self.num_list_string, ".*(.)]")
        self.assertEqual(output_regex1, '6')
        output_regex2 = regex_pp(self.num_list_string, ".*](.)")
        self.assertEqual(output_regex2, "")


class ConcreteClassesTest(unittest.TestCase):

    def setUp(self) -> None:
        self.owlr2_onto = owlready2.get_ontology('file://' + 'tests/data/ontology.owl')
        self.ontology = self.owlr2_onto.load()
        self.classes = self.ontology.classes()
        self.properties = self.ontology.properties()

    def test_reader(self):
        reader = OntologyReader()
        onto = reader.get_ontology('tests/data/ontology.owl')
        self.assertEqual(onto, Ontology(self.owlr2_onto))

    def test_class_name(self):
        mock = DummyClass()
        cls = OntologyClass(mock)
        self.assertEqual(cls.name, '')
        mock.name = None
        self.assertEqual(cls.name, '')
        mock.name = ''
        self.assertEqual(cls.name, '')
        mock.name = 'Dummy'
        self.assertEqual(cls.name, 'Dummy')

    def test_class_is_a(self):
        mock = DummyClass()
        mock.is_a = list(self.classes)
        cls = OntologyClass(mock)
        for x in self.classes:
            temp = OntologyClass(x)
            parent_list = cls.is_a()
            if any(type(x) != OntologyClass for x in parent_list):
                self.fail()
            if temp not in parent_list:
                self.fail()

    def test_restrictions(self):
        reader = OntologyReader(lang='en')
        onto = reader.get_ontology('tests/data/ontology.owl').load()
        classes = onto.classes()
        pizza = classes[0]
        restrictions = pizza.restrictions()
        self.assertEqual(restrictions[0], 'ontology.main_topping_of.exactly(1, owl.Thing)')

    def test_class_prefLabel_en(self):
        reader = OntologyReader(lang='en')
        onto = reader.get_ontology('tests/data/ontology.owl').load()
        classes = onto.classes()
        topping = classes[1]
        self.assertEqual(topping.prefLabel, 'Topping')

    def test_class_prefLabel_de(self):
        reader = OntologyReader(lang='de')
        onto = reader.get_ontology('tests/data/ontology.owl').load()
        classes = onto.classes()
        topping = classes[1]
        self.assertEqual(topping.prefLabel, 'Belag')

    def test_class_prefLabel_missing(self):
        reader = OntologyReader(lang='fr')
        onto = reader.get_ontology('tests/data/ontology.owl').load()
        classes = onto.classes()
        topping = classes[1]
        self.assertEqual(topping.prefLabel, '')

    def test_class_label_en(self):
        reader = OntologyReader(lang='en')
        onto = reader.get_ontology('tests/data/ontology.owl').load()
        classes = onto.classes()
        topping = classes[1]
        self.assertEqual(topping.label, 'Topping')

    def test_class_label_de(self):
        reader = OntologyReader(lang='de')
        onto = reader.get_ontology('tests/data/ontology.owl').load()
        classes = onto.classes()
        topping = classes[1]
        self.assertEqual(topping.label, 'Belag')

    def test_class_label_missing(self):
        reader = OntologyReader(lang='fr')
        onto = reader.get_ontology('tests/data/ontology.owl').load()
        classes = onto.classes()
        topping = classes[1]
        self.assertEqual(topping.label, '')

    def test_property_name(self):
        mock = DummyClass()
        cls = OntologyProperty(mock)
        self.assertEqual(cls.name, '')
        mock.name = None
        self.assertEqual(cls.name, '')
        mock.name = ''
        self.assertEqual(cls.name, '')
        mock.name = 'Dummy'
        self.assertEqual(cls.name, 'Dummy')

    def test_property_prefLabel_en(self):
        reader = OntologyReader(lang='en')
        onto = reader.get_ontology('tests/data/ontology.owl').load()
        properties = onto.properties()
        price = properties[0]
        self.assertEqual(price.prefLabel, 'Price')

    def test_property_prefLabel_de(self):
        reader = OntologyReader(lang='de')
        onto = reader.get_ontology('tests/data/ontology.owl').load()
        properties = onto.properties()
        price = properties[0]
        self.assertEqual(price.prefLabel, 'Preis')

    def test_property_prefLabel_missing(self):
        reader = OntologyReader(lang='fr')
        onto = reader.get_ontology('tests/data/ontology.owl').load()
        properties = onto.properties()
        price = properties[0]
        self.assertEqual(price.prefLabel, '')

    def test_property_label_en(self):
        reader = OntologyReader(lang='en')
        onto = reader.get_ontology('tests/data/ontology.owl').load()
        properties = onto.properties()
        price = properties[0]
        self.assertEqual(price.label, 'Price')

    def test_property_label_de(self):
        reader = OntologyReader(lang='de')
        onto = reader.get_ontology('tests/data/ontology.owl').load()
        properties = onto.properties()
        price = properties[0]
        self.assertEqual(price.label, 'Preis')

    def test_property_label_missing(self):
        reader = OntologyReader(lang='fr')
        onto = reader.get_ontology('tests/data/ontology.owl').load()
        properties = onto.properties()
        price = properties[0]
        self.assertEqual(price.label, '')

    def test_onto_load(self):
        correct_onto = Ontology(self.owlr2_onto.load())
        actual_onto = Ontology(self.owlr2_onto).load()
        self.assertEqual(correct_onto, actual_onto)


class FileUtils(unittest.TestCase):

    def test_incorrect(self):
        with self.assertRaises(ValueError):
            get_file_extension('filename.ext', ['json', 'yaml'])
        with self.assertRaises(ValueError):
            get_file_extension('filename', ['json', 'yaml'])

    def test_correct(self):
        ext = get_file_extension('filename.ext', ['ext', 'json'])
        self.assertEqual(ext, 'ext')


def main():
    unittest.main()


if __name__ == '__main__':
    main()
