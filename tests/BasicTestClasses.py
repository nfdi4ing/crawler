from typing import List

from src.OntologyObjects.owlready2Wrappers.Ontology import AbstractOntology
from src.OntologyObjects.owlready2Wrappers.OntologyClass import AbstractOntologyClass
from src.OntologyObjects.owlready2Wrappers.OntologyProperty import AbstractOntologyProperty
from src.OntologyObjects.owlready2Wrappers.OntologyReader import AbstractOntologyReader
from src.UtilityClasses.Postprocessor import AbstractPostprocessor


class SimpleStringMultiplierPP(AbstractPostprocessor):
    def __call__(self, arg: str, args: str = None):
        return arg * 3


class BasicReader(AbstractOntologyReader):

    def __init__(self, ontology):
        self._ontology = ontology

    def get_ontology(self, base_iri) -> [AbstractOntology]:
        return self._ontology


class BasicClass(AbstractOntologyClass):
    OntologyClass = None

    def __init__(self, parents: List, children: List, name: str, label="", pref_label="", restriction_list=None):
        if restriction_list is None:
            restriction_list = []
        self.name = name
        self.label = label
        self.prefLabel = pref_label
        self.parents = parents
        self.children = children
        self.restriction_list = restriction_list

    @property
    def prefLabel(self) -> str:
        return self._prefLabel

    @property
    def label(self) -> str:
        return self._label

    @property
    def name(self) -> str:
        return self._name

    def is_a(self) -> List:
        return [x for x in self.parents]

    def restrictions(self):
        return [x for x in self.restriction_list]

    def subclasses(self):
        return [x for x in self.children]

    def __eq__(self, other):
        if type(other) == BasicClass:
            return self.label == other.label and self.name == other.name and \
                   self.prefLabel == other.prefLabel
        else:
            return False

    def __hash__(self):
        to_be_hashed = self.name + self.label + self.prefLabel
        return hash(to_be_hashed)

    @prefLabel.setter
    def prefLabel(self, value):
        self._prefLabel = value

    @label.setter
    def label(self, value):
        self._label = value

    @name.setter
    def name(self, value):
        self._name = value


class BasicProperty(AbstractOntologyProperty):

    def __init__(self, domain: List[BasicClass], range: List[BasicClass], name: str, label="", prefLabel=""):
        self.domain = domain
        self.range = range
        self.name = name
        self.label = label
        self.prefLabel = prefLabel

    def get_domain(self):
        return [x for x in self.domain]

    def get_range(self):
        return [x for x in self.range]

    @property
    def prefLabel(self) -> str:
        return self._prefLabel

    @property
    def label(self) -> str:
        return self._label

    @property
    def name(self) -> str:
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @label.setter
    def label(self, value):
        self._label = value

    @prefLabel.setter
    def prefLabel(self, value):
        self._prefLabel = value


class BasicOntology(AbstractOntology):

    def __init__(self, classes=None, properties=None):
        if properties is None:
            properties = []
        if classes is None:
            classes = []
        self._classes = classes
        self._properties = properties

    def classes(self) -> List[AbstractOntologyClass]:
        return [x for x in self._classes]

    def properties(self) -> List[AbstractOntologyProperty]:
        return [x for x in self._properties]

    def load(self) -> 'AbstractOntology':
        return BasicOntology(classes=self.classes(), properties=self.properties())
