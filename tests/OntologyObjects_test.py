import unittest
import unittest.mock
import io
import os

from .BasicTestClasses import (
    BasicClass,
    BasicProperty,
    BasicReader,
    BasicOntology,
    SimpleStringMultiplierPP,
)

from src.OntologyObjects.OntologyExtractor import OntologyExtractor
from src.OntologyObjects.OntologyBuilder import OntologyBuilder
from src.OntologyObjects.OntologyHandler import OntologyHandler
from src.UtilityClasses.FileIOWrappers import JsonIO
from src.UtilityClasses.Parser import RegexParser, BasicStringParser, OSParser
from src.OntologyObjects.utils import get_label
from tests.Constants import *

"""
Setup Ontology by the example of:
https://de.wikipedia.org/wiki/Ontologie_(Informatik)#/media/Datei:Ontschichten.svg
"""
correct_filled_ontology = {
    "Data Quality": {
        "data_quality:_acurracy": "Roe",
    },
    "File not found entity": {"data_quality:file_not_found": ""},
    "Roundings": {
        "data_quality:_roundings": "Roe",
        "os_command_try": "This is an os command\n",
    },
    "Grid Generating Tools / Mesh Tools": {
        "Grid_Generating_Tools_/_Mesh_Tools:_selfmade_tool": "RungeKutta3TVD",
        "Grid_Generating_Tools_/_Mesh_Tools:_Version": "",
    },
    'Load Balacing': {
        'load_balancing:_category': '',
    },
}
correct_entity_dict = {
    "data_quality:_acurracy": "Roe",
}
correct_entity_dict2 = {"data_quality:file_not_found": ""}
""" Help variables for testing purposes"""
""" Setup OntologyUtil variables to be tested """
# JsonIO
json_io = JsonIO()

# OntologyHandler
data_path = os.path.join(os.path.dirname(__file__), "data")
json_data = json_io.load(os.path.join(data_path, "data.json"))

handler = OntologyHandler(json_data)

# OntologyReader
ontology = BasicOntology(classes, properties)
ontology_reader = BasicReader(ontology)
# Extractor
extractor = OntologyExtractor(ontology_reader)
# OntologyBuilder
builder = OntologyBuilder()


class TestOntologyBuilder(unittest.TestCase):

    def test_build(self):
        instance_property_dict = builder.build(class_property_dict)
        expected_items = init_classes_dict.items()
        for key, value in instance_property_dict.items():
            assert (
                key,
                value,
            ) in expected_items, (
                f"Pair ({key}, {value}) is not in the expected dictionary!"
            )
        assert len(init_classes_dict) == len(instance_property_dict)

    def test_get_label(self):
        test_list = [
            (name_prop, "name"),
            (vorname_prop, "vorname"),
            (benutzt, "benutzt"),
            (schlaegt, "schlaegt"),
            (geschlagen_von, "geschlagen von"),
            (kuenstler, "kuenstler"),
            (museum, "museum"),
            (bild, "bild_prefLabel"),
            (skulptur, "skulptur_prefLabel"),
        ]
        for prop, label in test_list:
            actual_label = get_label(prop)
            assert (
                actual_label == label
            ), f'Prop label should be {label}, but was "{actual_label}".'


class TestOntologyExtractor(unittest.TestCase):

    def test_add_to_prop_dict(self):
        class_prop_dict = {}
        # Test with class in its domain
        extractor.add_to_prop_dict(class_prop_dict, bild, name_prop)
        extractor.add_to_prop_dict(class_prop_dict, technik, name_prop)
        assert class_prop_dict[bild] == [
            name_prop
        ], "Bild class not associated with name property although it should be."
        assert class_prop_dict[technik] == [
            name_prop
        ], "Technik class not associated with name property although it should be."
        # Test with class outside its domain
        extractor.add_to_prop_dict(class_prop_dict, kubist, geschlagen_von)
        assert (
            kubist not in class_prop_dict
        ), 'Kubist class associated with "geschlagen_von" property although it should not be.'

    def test_get_onto_properties(self):
        onto_props = extractor.get_onto_properties(ontology)
        assert name_prop in onto_props, "Name not found in properties list."
        assert vorname_prop in onto_props, "Vorname not found in properties list."
        assert benutzt in onto_props, "Benutzt not found in properties list."
        assert schlaegt in onto_props, "Schlaegt not found in properties list."
        assert (
            geschlagen_von in onto_props
        ), "Geschlagen_von not found in properties list."
        assert (
            no_domain_property not in onto_props
        ), '"No domain property" found in properties list'

    def test_get_classes_with_properties(self):
        actual_get_classes_with_properties_dict = extractor.get_classes_with_properties(
            ontology
        )
        assert (
            actual_get_classes_with_properties_dict == class_property_dict
        ), "get_classes_with_properties method failed!"

    def test_extract(self):
        actual_extract_dict = extractor.extract("")
        expected_items = class_property_dict.items()
        for key, value in actual_extract_dict.items():
            assert (
                key,
                value,
            ) in expected_items, f"Pair ({get_label(key)}, {list(map(get_label, value))}) is not in the expected dictionary!"
        assert len(actual_extract_dict) == len(class_property_dict)

    def test_fill_prop_dict(self):
        class_prop_dict = extractor.fill_prop_dict(classes, properties)
        # Copy class property dict global variable and remove kubist because kubist has no properties.
        expected_items = class_property_dict.items()
        for key, value in class_prop_dict.items():
            assert (
                key,
                value,
            ) in expected_items, f"Pair ({get_label(key)}, {list(map(get_label, value))}) are not in the expected dictionary!"
        assert len(class_prop_dict) == len(class_property_dict)


class TestOntologyHandler(unittest.TestCase):

    @unittest.mock.patch("sys.stdout", new_callable=io.StringIO)
    def assert_stdout(self, fn, args, expected_output, mock_stdout):
        res = fn(*args)
        self.assertEqual(mock_stdout.getvalue(), expected_output)
        return res

    def test_read_and_match(self):
        path = os.path.abspath("tests/data/1D-LevelsetSodX.txt")
        fail_path = os.path.abspath("tests/data/data.json")

        missing_filepath = os.path.abspath("tests/data/1D-LevelsetSodX4125122.txt")
        pattern_type_list = ["RegexParser", "BasicStringParser", "OSParser"]
        pattern_list = [
            "\\|\\* Flux Splitting Scheme \\s*: ([\\w\\-]+)\\s*\\*\\|",
            "\\|\\* Time integrator\\s*: ([\\w\\-]+)\\s*\\*\\|",
            "this should be returned as a plain string",
            "echo 'This is an os command'",
        ]
        results = [
            "Roe",
            "RungeKutta3TVD",
            "this should be returned as a plain string",
            "",
            "This is an os command\n",
            "RoeRoeRoe",
        ]
        simple_pp_dict = {"type": "simple", "args": ""}
        pp_dict = {"type": "", "args": ""}
        match = handler.read_and_match("","",
            path, pattern_type_list[0], pattern_list[0], pp_dict
        )
        assert match == results[0], (
            "Match found was " + match + " but should have been " + results[0]
        )
        match = handler.read_and_match("", "",
            path, pattern_type_list[0], pattern_list[1], pp_dict
        )
        assert match == results[1], (
            "Match found was " + match + " but should have been " + results[1]
        )
        match = handler.read_and_match("","",
            path, pattern_type_list[1], pattern_list[2], pp_dict
        )
        assert match == results[2], (
            "Match found was " + match + " but should have been " + results[2]
        )
        
        match = handler.read_and_match("","",
            "", pattern_type_list[2], pattern_list[3], pp_dict
        )
        assert match == results[4], (
            "Match found was " + match + " but should have been " + results[3]
        )

        # the postprocessor dict has been removed from the function call.
        # match = handler.read_and_match("", "",
        #     path, pattern_type_list[0], pattern_list[0], simple_pp_dict
        # )
        # assert match == results[5], (
        #     "Match found was " + match + " but should have been " + results[0]
        # )

    def test_crawl_entity_info(self):
        actual_entity_dict = handler.crawl_entity_info("Data Quality")[0]

        assert (
            actual_entity_dict == correct_entity_dict
        ), "crawl_entity_info method returned unexpected values"

        actual_entity_dict = handler.crawl_entity_info("File not found entity")[0]

        assert (
            actual_entity_dict == correct_entity_dict2
        ), "crawl_entity_info method returned unexpected values "

    def test_handle(self):
        actual_filled_ontology = handler.handle()[0]

        assert (
            actual_filled_ontology == correct_filled_ontology
        ), "Filled ontology mismatch @ handle() function.\n" "Should have been:" + str(
            correct_filled_ontology
        ) + "\nBut was: " + str(
            actual_filled_ontology
        )

class SynonymTest(unittest.TestCase):
    def test_synonym(self):
        handler = OntologyHandler(json_data, config={"synonyms": [{"group": ["Load balancing", "Test-Synonym"]}]})
        actual_filled_ontology = handler.handle()[0]
        assert(actual_filled_ontology["Load Balacing"]["load_balancing:_category"] != ""), "Synonym not working correctly."

def main():
    unittest.main()


if __name__ == "__main__":
    main()
