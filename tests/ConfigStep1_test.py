import unittest

from tests.Constants import *
from tests.BasicTestClasses import BasicReader
from src.OntologyObjects.OntologyBuilder import OntologyBuilder
from src.OntologyObjects.OntologyExtractor import OntologyExtractor


"""
Setup Ontology by the example of:
https://de.wikipedia.org/wiki/Ontologie_(Informatik)#/media/Datei:Ontschichten.svg
"""

ontology_reader = BasicReader(ontology)
# OntologyBuilder
builder = OntologyBuilder()


class TestConfigStep1(unittest.TestCase):
    """! This class tests all config options related to the OntologyBuilder and OntologyExtractor (formerly known as step 2)."""

    def test_no_filter(self):
        """! Test with a config, that supplies neither "include_classes" nor "exclude_classes"."""
        config = {"museum": {"count": 2, "counts": {"name": [2, 1]}}}

        try:
            no_filter_extractor = OntologyExtractor(ontology_reader, config)
            assert False
        except ValueError:
            assert True

    def test_include(self):
        """! Test with a config, that supplies "include_classes"."""
        config = {"include_classes": ["bild_prefLabel"]}

        include_ontology_extractor = OntologyExtractor(ontology_reader, config)
        include_test_dict = include_ontology_extractor.extract(config)
        assert (
            len(include_test_dict.keys()) == 1
        ), f"Returned dictionary contains more/less than one entry! The returned keys are:{list(map(lambda x: x.name, include_test_dict.keys()))}"
        for k in include_test_dict:
            assert (
                k == bild
            ), "No other key except for skulptur_prefLabel was expected to be in the returned result!"

    def test_exclude(self):
        """! Test with a config, that supplies "exclude_classes"."""
        expected_dict = {
            museum: [name_prop],
            kubist: [name_prop, vorname_prop],
            technik: [name_prop],
            plastik: [name_prop],
            maler: [name_prop, vorname_prop]
        }
        config = {"exclude_classes": ["bildhauer", "bild_prefLabel"]}

        exclude_ontology_extractor = OntologyExtractor(ontology_reader, config)
        exclude_test_dict = exclude_ontology_extractor.extract(config)
        for k in exclude_test_dict:
            assert any(
                k == expected_key for expected_key in expected_dict
            ), f"Class {k.name} was not expected to be in the returned dictionary!"

    def test_empty_exclude(self):
        """! Test with a config, that supplies an empty "exclude_classes" list i.e. all classes are to be included."""
        expected_dict = {
            bildhauer: [name_prop, vorname_prop, schlaegt],
            skulptur: [name_prop, geschlagen_von],
            plastik: [name_prop, geschlagen_von],
            museum: [name_prop],
            bild: [name_prop, benutzt],
            kubist: [name_prop, vorname_prop],
            technik: [name_prop],
        }
        config = {"exclude_classes": []}

        empty_exclude_ontology_extractor = OntologyExtractor(ontology_reader, config)
        empty_exclude_dict = empty_exclude_ontology_extractor.extract(config)
        for k in empty_exclude_dict:
            assert any(k == expected_key for expected_key in expected_dict)

    def test_default_count(self):
        """! Test with a config, that supplies a global default_count property."""
        expected_dict = {"museum": {"__count__": 2}}
        config = {"include_classes": ["museum"], "default_count": 2, "default_prop_count": [0]}

        extractor = OntologyExtractor(ontology_reader, config)
        extracted_dict = extractor.extract(config)
        preprocessing_cache = builder.preprocessing(extracted_dict, config)

        default_count_dict = builder.build(extracted_dict, preprocessing_cache)
        try:
            assert default_count_dict["museum"]["__count__"] == 2
        except KeyError:
            assert False

    def test_inherit_count(self):
        """! Test with a config, that supplies a count to be inherited by a given child."""
        expected_dict = {"kubist": {"__count__": 2}}
        config = {
            "include_classes": ["kubist"],
            "default_count": 1,
            "default_prop_count": [0],
            "kuenstler": {"count": 2},
        }

        extractor = OntologyExtractor(ontology_reader, config)
        extracted_dict = extractor.extract(config)
        preprocessing_cache = builder.preprocessing(extracted_dict, config)

        inherit_count_dict = builder.build(extracted_dict, preprocessing_cache)
        try:
            assert (
                inherit_count_dict["kubist"]["__count__"]
                == expected_dict["kubist"]["__count__"]
            )
        except KeyError:
            assert False

    def test_local_count(self):
        """! Test with a config, that overwrites a global default_count with an entity local one."""
        expected_dict = {"museum": {"__count__": 2}, "bild_prefLabel": {"__count__": 5}}
        config = {
            "include_classes": ["museum", "bild_prefLabel"],
            "default_count": 2,
            "default_prop_count": [0],
            "bild_prefLabel": {"count": 5},
        }

        extractor = OntologyExtractor(ontology_reader, config)
        extracted_dict = extractor.extract(config)
        preprocessing_cache = builder.preprocessing(extracted_dict, config)

        local_count_dict = builder.build(extracted_dict, preprocessing_cache)
        try:
            assert local_count_dict["museum"]["__count__"] == 2
            assert local_count_dict["bild_prefLabel"]["__count__"] == 5
        except KeyError:
            assert False

    def test_no_count(self):
        """! Test with a config, that has neither a global default_count, nor any other count supplied."""
        config = {
            "include_classes": ["museum"],
        }

        extractor = OntologyExtractor(ontology_reader, config)
        extracted_dict = extractor.extract(config)
        preprocessing_cache = builder.preprocessing(extracted_dict, config)

        try:
            no_count_dict = builder.build(extracted_dict, preprocessing_cache)
            assert False
        except ValueError:
            assert True

    def test_default_prop_count(self):
        """! Test with a config, that supplies a global default_prop_count"""
        expected_dict = {"museum": {"__count__": 1, "name": [2]}}
        config = {
            "include_classes": ["museum"],
            "default_count": 1,
            "default_prop_count": [2],
        }

        extractor = OntologyExtractor(ontology_reader, config)
        extracted_dict = extractor.extract(config)
        preprocessing_cache = builder.preprocessing(extracted_dict, config)

        default_prop_count_dict = builder.build(extracted_dict, preprocessing_cache)
        try:
            assert (
                default_prop_count_dict["museum"]["name"]
                == expected_dict["museum"]["name"]
            )
        except KeyError:
            assert False

    def test_inherit_default_prop_count(self):
        """! Test with a config, that supplies a parent class with a default_prop_count"""
        expected_dict = {"kubist": {"__count__": 1, "name": [2]}}
        config = {
            "include_classes": ["kubist"],
            "default_count": 1,
            "kuenstler":{"default_prop_count": [2]},
        }

        extractor = OntologyExtractor(ontology_reader, config)
        extracted_dict = extractor.extract(config)
        preprocessing_cache = builder.preprocessing(extracted_dict, config)

        default_prop_count_dict = builder.build(extracted_dict, preprocessing_cache)
        try:
            assert (
                default_prop_count_dict["kubist"]["name"]
                == expected_dict["kubist"]["name"]
            )
        except KeyError:
            assert False

    def test_inherit_prop_count(self):
        """! Test with a config, wherein a property count is inherited by a child class."""
        expected_dict = {"kubist": {"__count__": 1, "name": [1]}}
        config = {
            "include_classes": ["kubist"],
            "default_count": 1,
            "default_prop_count": [0],
            "kuenstler": {"counts": {"name": [1]}},
        }

        extractor = OntologyExtractor(ontology_reader, config)
        extracted_dict = extractor.extract(config)
        preprocessing_cache = builder.preprocessing(extracted_dict, config)

        inherited_prop_count_dict = builder.build(extracted_dict, preprocessing_cache)
        try:
            assert (
                inherited_prop_count_dict["kubist"]["name"]
                == expected_dict["kubist"]["name"]
            )
        except KeyError:
            assert False

    def test_local_prop_count(self):
        """! Test with a config, that supplies an overwriting property local count."""
        expected_dict = {"kubist": {"__count__": 1, "name": [2]}}
        config = {
            "include_classes": ["kubist"],
            "default_count": 1,
            "default_prop_count": [0],
            "kuenstler": {"counts": {"name": [5]}},
            "kubist": {"counts": {"name": [2]}},
        }

        extractor = OntologyExtractor(ontology_reader, config)
        extracted_dict = extractor.extract(config)
        preprocessing_cache = builder.preprocessing(extracted_dict, config)

        local_prop_count_dict = builder.build(extracted_dict, preprocessing_cache)
        try:
            assert (
                local_prop_count_dict["kubist"]["name"]
                == expected_dict["kubist"]["name"]
            )
        except KeyError:
            assert False
    
    def test_ignore_property(self):
        """! Test with a config, that supplies a count of [0] to a property, thus ignoring it."""
        expected_dict = {"kubist": {"__count__": 1}}
        config = {
            "include_classes": ["kubist"],
            "default_count": 1,
            "default_prop_count": [0],
            "kuenstler": {"counts": {"name": [5]}},
            "kubist": {"counts": {"name": [0]}},
        }

        extractor = OntologyExtractor(ontology_reader, config)
        extracted_dict = extractor.extract(config)
        preprocessing_cache = builder.preprocessing(extracted_dict, config)

        ignore_prop_dict = builder.build(extracted_dict, preprocessing_cache)
        try:
            ignore_prop_dict["kubist"]["name"]
            assert False
        except KeyError:
            assert True

    def test_add_new_prop(self):
        """! Test with a config, that introduces a new property to the entity."""
        expected_dict = {"kubist": {"__count__": 1, "shoes": [2]}}
        config = {
            "include_classes": ["kubist"],
            "default_count": 1,
            "default_prop_count": [0],
            "kubist": {"counts": {"shoes": [2]}},
        }

        extractor = OntologyExtractor(ontology_reader, config)
        extracted_dict = extractor.extract(config)
        preprocessing_cache = builder.preprocessing(extracted_dict, config)

        add_new_prop_dict = builder.build(extracted_dict, preprocessing_cache)
        try:
            assert (
                add_new_prop_dict["kubist"]["shoes"] == expected_dict["kubist"]["shoes"]
            )
        except KeyError:
            assert False

    def test_ignore_new_prop(self):
        """! Test with a config, that supplies a count of zero to a new property, thus ignoring it."""
        expected_dict = {"kubist": {"__count__": 1}}
        config = {
            "include_classes": ["kubist"],
            "default_count": 1,
            "default_prop_count": [2],
            "kubist": {"counts": {"shoes": [0]}},
        }

        extractor = OntologyExtractor(ontology_reader, config)
        extracted_dict = extractor.extract(config)
        preprocessing_cache = builder.preprocessing(extracted_dict, config)

        ignore_new_prop_dict = builder.build(extracted_dict, preprocessing_cache)
        try:
            ignore_new_prop_dict["kubist"]["shoes"]
            assert False
        except KeyError:
            assert True

    def test_inherit_new_prop(self):
        """! Test with a config, that supplies a new property to be inherited by a given child."""
        expected_dict = {"kubist": {"__count__": 1, "shoes": [1]}}
        config = {
            "include_classes": ["kubist"],
            "default_count": 1,
            "default_prop_count": [2],
            "kuenstler": {"counts": {"shoes": [1]}},
        }

        extractor = OntologyExtractor(ontology_reader, config)
        extracted_dict = extractor.extract(config)
        preprocessing_cache = builder.preprocessing(extracted_dict, config)

        inherit_new_prop_dict = builder.build(extracted_dict, preprocessing_cache)
        try:
            assert (
                inherit_new_prop_dict["kubist"]["shoes"]
                == expected_dict["kubist"]["shoes"]
            )
        except KeyError as ke:
            print(ke)
            assert False

    def test_inherit_count_depth_2(self):
        """! Test with a config, that supplies a count to be inherited to the grandparent of a given class."""
        expected_dict = {"plastik": {"__count__": 20}}
        config = {
            "include_classes": ["plastik"],
            "default_count": 1,
            "default_prop_count": [0],
            "kunstwerk": {"count": 20},
        }

        extractor = OntologyExtractor(ontology_reader, config)
        extracted_dict = extractor.extract(config)
        preprocessing_cache = builder.preprocessing(extracted_dict, config)

        inheritance_count_depth_2_dict = builder.build(extracted_dict, preprocessing_cache)
        try:
            assert (
                inheritance_count_depth_2_dict["plastik"]["__count__"]
                == expected_dict["plastik"]["__count__"]
            )
        except KeyError:
            assert False

    def test_inherit_prop_count_depth_2(self):
        """! Test with a config, that supplies a property count to be inherited to the grandparent of a given class."""
        expected_dict = {"plastik": {"__count__": 1, "name": [5]}}
        config = {
            "include_classes": ["plastik"],
            "default_count": 1,
            "default_prop_count": [0],
            "kunstwerk": {"counts": {"name": [5]}},
        }

        extractor = OntologyExtractor(ontology_reader, config)
        extracted_dict = extractor.extract(config)
        preprocessing_cache = builder.preprocessing(extracted_dict, config)

        inheritance_depth_2_dict = builder.build(extracted_dict, preprocessing_cache)
        try:
            assert (
                inheritance_depth_2_dict["plastik"]["name"]
                == expected_dict["plastik"]["name"]
            )
        except KeyError:
            assert False

    def test_inherit_new_prop_count_depth_2(self):
        """! Test with a config, that supplies a new property to be inherited to the grandparent of a given class."""
        expected_dict = {"plastik": {"__count__": 1, "evaluation": [5]}}
        config = {
            "include_classes": ["plastik"],
            "default_count": 1,
            "default_prop_count": [0],
            "kunstwerk": {"counts": {"evaluation": [5]}},
        }

        extractor = OntologyExtractor(ontology_reader, config)
        extracted_dict = extractor.extract(config)
        preprocessing_cache = builder.preprocessing(extracted_dict, config)

        inheritance_new_prop_depth_2_dict = builder.build(extracted_dict, preprocessing_cache)
        try:
            assert (
                inheritance_new_prop_depth_2_dict["plastik"]["evaluation"]
                == expected_dict["plastik"]["evaluation"]
            )
        except KeyError:
            assert False


def main():
    unittest.main()


if __name__ == "__main__":
    main()
