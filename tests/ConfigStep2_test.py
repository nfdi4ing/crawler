import unittest
from tests.Constants import step2_config, ontology
from tests.BasicTestClasses import BasicReader

from src.OntologyObjects.OntologyBuilder import OntologyBuilder
from src.OntologyObjects.OntologyExtractor import OntologyExtractor
from src.OntologyObjects.OntologyMultiplexer import OntologyMultiplexer
from src.OntologyObjects.utils import (
    get_label,
    determine_crawler_attributes,
    dimensionality_of_list,
)
extracted_dict = OntologyExtractor(BasicReader(ontology), step2_config).extract(step2_config)
builder = OntologyBuilder()
build_cache = builder.preprocessing(extracted_dict, step2_config)
data_dict = OntologyBuilder().build(extracted_dict, build_cache)

label_class_dict = {}
for owl_class in BasicReader(ontology).get_ontology("").load().classes():
    label_class_dict[get_label(owl_class)] = owl_class


class TestConfigStep2(unittest.TestCase):

    def test_dimensionality_of_list(self):
        # trivial
        dim1 = []
        assert dimensionality_of_list(dim1) == 1
        dim1 = ["path"]
        assert dimensionality_of_list(dim1) == 1
        # toml only allows for list of lists and no mixed lists, so this is enough
        dim2 = [["path"]]
        assert dimensionality_of_list(dim2) == 2
        # but toml allows for lists of lists like this
        dim3 = [["path"], [["path", "path"], ["path"]]]
        assert dimensionality_of_list(dim3) == 3

    def test_determine_crawl_properties(self):
        dimensionality_vorname = dimensionality_of_list(
            step2_config["kubist"]["paths"]["vorname"]
        )
        # Andrejewna
        expected_property = step2_config["kubist"]["paths"]["vorname"][0][1]
        assert expected_property == determine_crawler_attributes(
            "kubist", ["paths", "vorname"], step2_config, dimensionality_vorname, 0, 1
        )
        # Pablo
        expected_property = step2_config["kubist"]["paths"]["vorname"][1]
        assert expected_property == determine_crawler_attributes(
            "kubist", ["paths", "vorname"], step2_config, dimensionality_vorname, 1
        )

        dimensionality_geschlagen_von = dimensionality_of_list(
            step2_config["skulptur_prefLabel"]["paths"]["geschlagen von"]
        )
        # skulptur_path_geschlagen_von2
        expected_property = step2_config["skulptur_prefLabel"]["paths"][
            "geschlagen von"
        ][1]
        assert expected_property == determine_crawler_attributes(
            "skulptur_prefLabel",
            ["paths", "geschlagen von"],
            step2_config,
            dimensionality_geschlagen_von,
            1,
        )

    def test_multiplexer_preprocessing_preparing(self):
        expected_preprocessing_cache = {
            "kubist": {
                "paths": {"name": [], "vorname": ['kubist']},
                "path": [],
                "type": [],
                "types": {"vorname": [], "name": []},
                "pattern": [],
                "patterns": {"vorname": [], "name": []},
                "postprocessor": [],
                "postprocessors": {"vorname": [], "name": []}
            },
            "plastik": {
                "paths": {"name": [], "geschlagen von": []},
                "path": [],
                "type": [],
                "types": {"name": [], "geschlagen von": []},
                "pattern": [],
                "patterns": {"name": [], "geschlagen von": []},
                "postprocessor": [],
                "postprocessors": {"name": [], "geschlagen von": []}
            },
        }

        expected_entity_properties_to_search = {
            "kubist": {
                "paths": ["name"],
                "types": ["name", "vorname"],
                "patterns": ["name", "vorname"],
                "postprocessors": ["name", "vorname"]
            },
            "plastik": {
                "paths": ["name", "geschlagen von"],
                "types": ["name", "geschlagen von"],
                "patterns": ["name", "geschlagen von"],
                "postprocessors": ["name", "geschlagen von"]
            },
        }

        actual_entites, actual_cache = (
            OntologyMultiplexer.preprocessing_preparing(
                data_dict, step2_config
            )
        )

        for entity in actual_entites:
            assert (
                actual_entites[entity] == expected_entity_properties_to_search[entity]
            )

        assert actual_entites == expected_entity_properties_to_search

        for entity in actual_cache:
            try:
                assert (
                    actual_cache[entity]["paths"].keys()
                    == expected_preprocessing_cache[entity]["paths"].keys()
                )
                assert (
                    actual_cache[entity]["path"]
                    == expected_preprocessing_cache[entity]["path"]
                )
            except KeyError as ke:
                assert False, ke

        assert actual_cache == expected_preprocessing_cache

    def test_multiplexer_preprocessing(self):
        expected_cache = {
            "plastik": {
                "paths": {"name": [], "geschlagen von": ["skulptur_prefLabel"]},
                "path": ["plastik", "kunstwerk"],
                "type": [],
                "types": {"name": [], "geschlagen von": []},
                "pattern": [],
                "patterns": {"name": [], "geschlagen von": []},
                "postprocessor": [],
                "postprocessors": {"name": [], "geschlagen von": []}
            },
            "kubist": {
                "paths": {"name": [], "vorname": ['kubist']},
                "path": ["kuenstler"],
                "pattern": [],
                "patterns": {"name": [], "vorname": []},
                "type": [],
                "types": {"name": [], "vorname": []},
                "postprocessor": [],
                "postprocessors": {"name": [], "vorname": []}
            },
        }

        # test parent_path_extraction alone
        actual_entities, actual_cache = (
            OntologyMultiplexer.preprocessing_preparing(
                data_dict, step2_config
            )
        )

        plastik_parents = ["plastik", "skulptur_prefLabel", "kunstwerk"]
        kubist_parents = ["kubist","kuenstler"]

        for parent in plastik_parents:
            OntologyMultiplexer.preprocessing_parent_extraction(
                "plastik", parent, step2_config, actual_entities, actual_cache
            )
        for parent in kubist_parents:
            OntologyMultiplexer.preprocessing_parent_extraction(
                "kubist", parent, step2_config, actual_entities, actual_cache
            )

        assert expected_cache == actual_cache

        # test actual bfs
        actual_cache = OntologyMultiplexer.preprocessing_bfs(
            data_dict, step2_config, label_class_dict
        )
        assert actual_cache == expected_cache

    def test_multiply_with_config_and_ontology(self):
        expected_multiplexer_dict = {
            "kubist_1": {
                "__restrictions__": [],
                "vorname_1": {
                    "path": {"paths": ["Nadeschda"], "path": []},
                    "pattern": [],
                    "postprocessor": [],
                    "type": [],
                },
                "vorname_2": {
                    "path": {"paths": ["Andrejewna"], "path": []},
                    "pattern": [],
                    "postprocessor": [],
                    "type": [],
                },
                "name": {
                    "path": {"paths": [], "path": ["Kubist"]},
                    "pattern": [],
                    "postprocessor": [],
                    "type": [],
                },
            },
            "kubist_2": {
                "__restrictions__": [],
                "vorname": {
                    "path": {"paths": ["Pablo"], "path": []},
                    "pattern": [],
                    "postprocessor": [],
                    "type": [],
                },
                "name": {
                    "path": {"paths": [], "path": ["Kubist"]},
                    "pattern": [],
                    "postprocessor": [],
                    "type": [],
                },
            },
            "kubist_3": {
                "__restrictions__": [],
                "vorname": {
                    "path": {"paths": ["Maria"], "path": []},
                    "pattern": [],
                    "postprocessor": [],
                    "type": [],
                },
                "name": {
                    "path": {"paths": [], "path": ["Kubist"]},
                    "pattern": [],
                    "postprocessor": [],
                    "type": [],
                },
            },
            "plastik_1": {
                "__restrictions__": [],
                "name": {
                    "path": {
                        "paths": [],
                        "path": [
                            "plastik_path1",
                            "plastik_path2",
                            "plastik_path3",
                            "kunstwerk_path1",
                        ],
                    },
                    "pattern": [],
                    "postprocessor": [],
                    "type": [],
                },
                "geschlagen von": {
                    "path": {"paths": ["skulptur_path_geschlagen_von1"], "path": []},
                    "pattern": [],
                    "postprocessor": [],
                    "type": [],
                },
            },
            "plastik_2": {
                "__restrictions__": [],
                "name_1": {
                    "path": {
                        "paths": [],
                        "path": [
                            "plastik_path1",
                            "plastik_path2",
                            "plastik_path3",
                            "kunstwerk_path1",
                        ],
                    },
                    "pattern": [],
                    "postprocessor": [],
                    "type": [],
                },
                "name_2": {
                    "path": {
                        "paths": [],
                        "path": [
                            "plastik_path1",
                            "plastik_path2",
                            "plastik_path3",
                            "kunstwerk_path1",
                        ],
                    },
                    "pattern": [],
                    "postprocessor": [],
                    "type": [],
                },
                "geschlagen von_1": {
                    "path": {"paths": ["skulptur_path_geschlagen_von2"], "path": []},
                    "pattern": [],
                    "postprocessor": [],
                    "type": [],
                },
                "geschlagen von_2": {
                    "path": {"paths": ["skulptur_path_geschlagen_von2"], "path": []},
                    "pattern": [],
                    "postprocessor": [],
                    "type": [],
                },
            },
        }
        preproc_cache = OntologyMultiplexer.preprocessing(data_dict, step2_config, label_class_dict)
        actual_dict = OntologyMultiplexer.multiply(
            data_dict, preproc_cache, step2_config
        )
        assert actual_dict == expected_multiplexer_dict

    def test_multiply_with_just_config(self):
        expected_multiplexer_dict = {
            "kubist_1": {
                "__restrictions__": [],
                "vorname_1": {
                    "path": {"paths": ["Nadeschda"], "path": []},
                    "pattern": [],
                    "postprocessor": [],
                    "type": [],
                },
                "vorname_2": {
                    "path": {"paths": ["Andrejewna"], "path": []},
                    "pattern": [],
                    "postprocessor": [],
                    "type": [],
                },
                "name": {
                    "path": {"paths": [], "path": ["Kubist"]},
                    "pattern": [],
                    "postprocessor": [],
                    "type": [],
                },
            },
            "kubist_2": {
                "__restrictions__": [],
                "vorname": {
                    "path": {"paths": ["Pablo"], "path": []},
                    "pattern": [],
                    "postprocessor": [],
                    "type": [],
                },
                "name": {
                    "path": {"paths": [], "path": ["Kubist"]},
                    "pattern": [],
                    "postprocessor": [],
                    "type": [],
                },
            },
            "kubist_3": {
                "__restrictions__": [],
                "vorname": {
                    "path": {"paths": ["Maria"], "path": []},
                    "pattern": [],
                    "postprocessor": [],
                    "type": [],
                },
                "name": {
                    "path": {"paths": [], "path": ["Kubist"]},
                    "pattern": [],
                    "postprocessor": [],
                    "type": [],
                },
            },
            "plastik_1": {
                "__restrictions__": [],
                "name": {
                    "path": {
                        "paths": [],
                        "path": [
                            "plastik_path1",
                            "plastik_path2",
                            "plastik_path3",
                        ],
                    },
                    "pattern": [],
                    "postprocessor": [],
                    "type": [],
                },
                "geschlagen von": {
                    "path": {"paths": ["skulptur_path_geschlagen_von1"], "path": []},
                    "pattern": [],
                    "postprocessor": [],
                    "type": [],
                },
            },
            "plastik_2": {
                "__restrictions__": [],
                "name_1": {
                    "path": {
                        "paths": [],
                        "path": [
                            "plastik_path1",
                            "plastik_path2",
                            "plastik_path3",
                        ],
                    },
                    "pattern": [],
                    "postprocessor": [],
                    "type": [],
                },
                "name_2": {
                    "path": {
                        "paths": [],
                        "path": [
                            "plastik_path1",
                            "plastik_path2",
                            "plastik_path3",
                        ],
                    },
                    "pattern": [],
                    "postprocessor": [],
                    "type": [],
                },
                "geschlagen von_1": {
                    "path": {"paths": ["skulptur_path_geschlagen_von2"], "path": []},
                    "pattern": [],
                    "postprocessor": [],
                    "type": [],
                },
                "geschlagen von_2": {
                    "path": {"paths": ["skulptur_path_geschlagen_von2"], "path": []},
                    "pattern": [],
                    "postprocessor": [],
                    "type": [],
                },
            },
        }
        preproc_cache = OntologyMultiplexer.preprocessing(data_dict, step2_config)
        actual_dict = OntologyMultiplexer.multiply(data_dict, preproc_cache, step2_config)

        assert actual_dict == expected_multiplexer_dict
