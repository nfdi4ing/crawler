from tests.BasicTestClasses import (
    BasicClass,
    BasicProperty,
    BasicOntology,
)

from toml import loads

"""Constants: """
kuenstler = BasicClass([], [], "kuenstler", "kuenstler", "kuenstler")
kunstwerk = BasicClass([], [], "kunstwerk", "kunstwerk", "kunstwerk")
skulptur = BasicClass([kunstwerk], [], "skulptur", label="skulptur_prefLabel")
plastik = BasicClass([skulptur], [], "plastik", "plastik", "plastik")
museum = BasicClass([], [], "museum")
bild = BasicClass([kunstwerk], [], "bild", "bild_label", "bild_prefLabel")
kubist = BasicClass([kuenstler], [], "kubist")
maler = BasicClass([kuenstler], [kubist], "maler")
technik = BasicClass([], [], "technik")
bildhauer = BasicClass([kuenstler], [], "bildhauer")
kuenstler.children = [kubist, maler, bildhauer]
kunstwerk.children = [skulptur, bild]
skulptur.children = [plastik]
dom_1 = [
    kuenstler,
    kunstwerk,
    museum,
    technik,
]
""" Properties: """
name_prop = BasicProperty(dom_1, [], "name")
vorname_prop = BasicProperty([kuenstler, bildhauer, maler], [], "vorname")
benutzt = BasicProperty([bild], [technik], "benutzt")
schlaegt = BasicProperty([bildhauer], [skulptur], "schlaegt")
geschlagen_von = BasicProperty([skulptur], [bildhauer], "geschlagen von")
no_domain_property = BasicProperty([], [], "no_domain_property")
classes = [
    kuenstler,
    bildhauer,
    maler,
    skulptur,
    bild,
    kunstwerk,
    museum,
    technik,
    kubist,
    plastik,
]
properties = [
    name_prop,
    vorname_prop,
    benutzt,
    schlaegt,
    geschlagen_von,
    no_domain_property,
]

# Ontology
ontology = BasicOntology(classes, properties)

"""Dictionaries"""
class_property_dict = {
    # kuenstler: [name_prop, vorname_prop],
    bildhauer: [name_prop, vorname_prop, schlaegt],
    # kunstwerk: [name_prop],
    # skulptur: [name_prop, geschlagen_von],
    museum: [name_prop],
    bild: [name_prop, benutzt],
    # maler: [name_prop, vorname_prop],
    kubist: [name_prop, vorname_prop],
    technik: [name_prop],
    plastik: [name_prop, geschlagen_von],
}

init_classes_dict = {
    "bildhauer": {
        "__count__": 1,
        "__is_subclass_of__": ["kuenstler"],
        "__restrictions__": [],
        "name": [1],
        "vorname": [1],
        "schlaegt": [1],
    },
    "plastik": {
        "__count__": 1,
        "__is_subclass_of__": ["skulptur_prefLabel"],
        "__restrictions__": [],
        "name": [1],
        "geschlagen von": [1],
    },
    "museum": {
        "__count__": 1,
        "__is_subclass_of__": [],
        "__restrictions__": [],
        "name": [1],
    },
    "bild_prefLabel": {
        "__count__": 1,
        "__is_subclass_of__": ["kunstwerk"],
        "__restrictions__": [],
        "name": [1],
        "benutzt": [1],
    },
    "technik": {
        "__count__": 1,
        "__is_subclass_of__": [],
        "__restrictions__": [],
        "name": [1],
    },
    "kubist": {
        "__count__": 1,
        "__is_subclass_of__": ["kuenstler"],
        "__restrictions__": [],
        "name": [1],
        "vorname": [1],
    },
}

step2_config = loads("""
include_classes = ["plastik", "kubist"]
plastik.count = 2
plastik.default_prop_count = [1,2]
plastik.path = ["plastik_path1", "plastik_path2", "plastik_path3"]

skulptur_prefLabel.paths."geschlagen von" = [["skulptur_path_geschlagen_von1"],["skulptur_path_geschlagen_von2"]]
kunstwerk.path = ["kunstwerk_path1"]

kubist.count = 3
kubist.counts.vorname = [2,1,1]

kubist.paths.vorname = [[["Nadeschda"], ["Andrejewna"]],["Pablo"],["Maria"]]
kuenstler.counts.name = [1,1,1]
kuenstler.path = ["Kubist"]
""")